\documentclass{article}

\usepackage{hyperref}
\usepackage{amsmath,amsfonts,amsthm,amssymb} \usepackage{setspace}
\usepackage{fancyhdr} \usepackage{lastpage} \usepackage{extramarks}
\usepackage{chngpage} \usepackage{soul}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{graphicx,float,wrapfig} \usepackage{ifthen}
\usepackage{listings} \usepackage{courier}
\usepackage{enumerate} % to customize list enumeration
\usepackage{txfonts}

\definecolor{MyDarkGreen}{rgb}{0.0,0.4,0.0}

\lstloadlanguages{Haskell,Lisp}%
\lstset{frame=single, basicstyle=\small\ttfamily,
  keywordstyle=[1]\color{Blue}\bf, keywordstyle=[2]\color{Purple},
  keywordstyle=[3]\color{Blue}\underbar, identifierstyle=,
  commentstyle=\usefont{T1}{pcr}{m}{sl}\color{MyDarkGreen}\small,
  stringstyle=\color{Purple}, showstringspaces=false, tabsize=5,
  morecomment=[l][\color{Blue}]{...}, numbers=left, firstnumber=1,
  numberstyle=\tiny\color{Blue},
  stepnumber=5,xleftmargin=-1.5cm,xrightmargin=-1.5cm }

\title{CSC 402 Final Project \\ Building A Scheme in 48 Hours}
\author{Chris Sims}

\begin{document}
\maketitle

\section{Background}
\label{sec:background}

For the final project of the semester for CSC 402, {\em Programming
  Language Implementation}, the stated goal was to implement a
compiler or interpreter for a target language of our choosing. To
present a greater challenge for my final project, I proposed following
the {\em Build A Scheme in 48 Hours}\footnote{
  \url{http://en.wikibooks.org/wiki/Write_Yourself_a_Scheme_in_48_Hours}}
book.

\subsection{Write A Scheme in 48 Hours}
\label{sec:write-scheme}

The book walks through a basic implementation of a Scheme\footnote{
  \url{http://www.schemers.org/Documents/Standards/R5RS/HTML/}}
interpreter, written in Haskell using the Parsec\footnote{
  \url{http://legacy.cs.uu.nl/daan/parsec.html}} parsing library. Its
goal is an introduction to both Haskell and parsing a language using
Parsec. It guides the reader through a graduated implementation,
starting with parsing the language and moving on to evaluation and
offering a Read-Eval-Print Loop (REPL). In each section, exercises are
offered to expand on the interpreter, and implement more of the Scheme
specification.

\subsection{Scheme}
\label{sec:scheme}

Scheme borrows heavily from its Lisp predecessor in being a list-based,
homoiconic, statically scoped programming language. It is a weakly
typed language that enjoys macros, tail recursion, and first-class
functions and continuations.

\section{Implementation}
\label{sec:implementation}

I'm rather fond of Lisp dialects, so this was a fun language to
implement. The Parsec library offers powerful tools to parse a target
language, and define a grammar. Using monads, it employs something
like a pipeline to attempt matching a parser token to a parsing
function. Since Scheme is a homoiconic language, it's actually quite
simple to parse. The syntax of the language is small, with list
composition being its main method of expressing semantics.

Monads were used frequently to handle everything from errors to IO,
and the book forced me to become familiar with using them, and moving
data between different monads.

The code listings for both the interpreter and the standard library
can be found in Section~\ref{sec:listings}. Listing~\ref{main} is the
simple entry to the interpreter, handling either an argument
representing a file to interpret, or no argument to start a
REPL. Listing~\ref{parse} contains the parsing mechanism and
represents the overall grammar of the language. As can be seen in the
listing, the language itself is quite simple, with only a handful of
primitive tokens to parse. Listing~\ref{eval} contains the bulk of the
interpreter: the dispatch {\tt eval} function, implementation of
the primitive functions, REPL interaction, and handling of the
stateful environment. Listing~\ref{stdlib} contains a small standard
library for the implemented language, and shows just how powerful
first-class functions and function composition can be.

\section{Challenges}
\label{sec:challenges}
It took a bit of work to get Emacs set up to properly check the
Haskell code as I typed it, as well as the autocomplete. However, this
helped tremendously during coding to catch subtle errors like
indentation, and to explore type signatures for functions.

The book was purposefully a bit sparse, but often either linked to the
standard documentation or required me to go out and find an
explanation for an expression before I moved on.

Not directly related to the final project itself but a large impact
nonetheless was my move across the country. The drive itself along
with all the preparation for it was a significant burden on the time
available to work on the project.

\section{Examples}
\label{sec:examples}
There are a large number of working examples in the Standard Library
listing in Listing~\ref{stdlib}. A screenshot of this library being
used along with language primitives can be seen in
Figure~\ref{fig:screenshot}.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.8\linewidth]{screenshot}
  \caption{Example Screenshot}
  \label{fig:screenshot}
\end{figure}

\section{Summary}
\label{sec:summary}

In spite of the project being a mostly guided journey into building a
parser and evaluator, it was quite enlightening. Haskell is anything
but an easy language to learn, and a non-negligible amount of time was
spent reading documentation on the Parsec library and the Haskell
syntax and standard libraries.

Scheme is an incredibly powerful language in its simplicity, and the
elegance of the standard library (Listing~\ref{stdlib}) exemplifies
this. With such a small amount of code in the library, and a small set
of primitives implemented in the interpreter, powerful tools such as
map and fold are available for use.

In my proposal I had suggested that I might be able to expand on the
implemented language with something like hygienic
macros. Unfortunately, with the end of the term being as tight as it
is, I was unable to tackle that addition to the language.

\section{Listings}
\label{sec:listings}

\lstinputlisting[language=Haskell,caption=Main module,label=main]{simpleParser.hs}
\lstinputlisting[language=Haskell,caption=Types,label=types]{Lisp/Types.hs}
\lstinputlisting[language=Haskell,caption=Parsing,label=parse]{Lisp/Parse.hs}
\lstinputlisting[language=Haskell,caption=Evaluation,label=eval]{Lisp/Eval.hs}

\lstinputlisting[language=Lisp,caption=Standard library for our
interpreted language, morekeywords=
{define,if,func,car,cdr,lambda},label=stdlib] {stdlib.scm}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
