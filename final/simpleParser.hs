module Main where

import Lisp.Eval
import System.Environment

-- If no argument is passed, start a REPL
-- If an argument is passed, interpret the passed filename
main :: IO ()
main = do args <- getArgs
          if null args then runRepl else runOne args
