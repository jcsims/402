grammar hw_5;

//********************************************************
// simple1 scripting language grammar
// Version 2.0

@header {
import java.util.HashMap;
import java.util.Set;
import java.util.Iterator;
import java.util.Map;
}
@members{
    public static HashMap usageMap = new HashMap();

    public void reportError(RecognitionException e) {
        // call the Parser member function to report the error
        displayRecognitionError(this.getTokenNames(), e);
        // exit
        System.exit(1);
    }
}

prog returns [AST ast]
    :   { $ast = new StmtList(); } (stmt {$ast.addAST($stmt.ast);})+
    ;

stmt returns [Stmt ast]
    :   VAR '=' exp ';'?     { $ast = new AssignStmt($VAR.text,$exp.ast); }
    |   'get' VAR ';'?       { $ast = new GetStmt($VAR.text); }
    |   'put' exp ';'?       { $ast = new PutStmt($exp.ast); }
    |   'while' '(' exp ')' s=stmt { $ast = new WhileStmt($exp.ast,$s.ast); }
    |   'if' '(' exp ')' s1=stmt
        { $ast = new IfStmt($exp.ast,$s1.ast); }
        ( 'else' s2=stmt { $ast.addAST($s2.ast); } )?
    |   '{' { $ast = new BlockStmt(); }
        (s=stmt {$ast.addAST($s.ast);})+
        '}'
    ;

exp returns [Expr ast]
    :   relexp {$ast = $relexp.ast; };

relexp returns [Expr ast]
    :   e1=addexp { $ast = $e1.ast; }
        (
            ('==' e2=addexp { 
                    $ast = new BinopExpr(BinopExpr.EQ,$ast,$e2.ast); })|
            ('<=' e3=addexp { 
                    $ast = new BinopExpr(BinopExpr.LESSEQ,$ast,$e3.ast); })
        )*
    ;

addexp returns [Expr ast]
    :   e1=mulexp { $ast = $e1.ast; }
        (
            ('+' e2=mulexp { 
                    $ast = new BinopExpr(BinopExpr.ADD,$ast,$e2.ast); })|
            ('-' e3=mulexp { 
                    $ast = new BinopExpr(BinopExpr.MINUS,$ast,$e3.ast); })
        )*
    ;

mulexp returns [Expr ast]
    :   e1=atom { $ast = $e1.ast; }
        (
            ('*' e2=atom { 
                    $ast = new BinopExpr(BinopExpr.MULT,$ast,$e2.ast); })|
            ('/' e3=atom { $ast = new BinopExpr(BinopExpr.DIV,$ast,$e3.ast); })
        )*
    ;

atom returns [Expr ast]
    :   '(' exp ')'     { $ast = new ParenExpr($exp.ast); }
    | VAR { $ast = new VarExpr($VAR.text);
            usageMap.put($VAR.text,new Boolean(true)); }
    |   NUM     { $ast = new NumExpr($NUM.text); }
    |   '-' NUM     { $ast = new NumExpr('-' + $NUM.text); }
    ;

//*************************************************************************
// lexical analyzer stuff
// NOTE: putting negative number recognition into the lexer does
// not work, hides the minus sign from the parser and this leads
// to syntax error, need to expose the minus sign at the rule level, see above
VAR     :   ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')* ;
NUM     :   '0'..'9'+;
COMMENT :       '//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;};
WS      :       ( ' ' | '\t' | '\r' | '\n' ) {$channel=HIDDEN;};
STRING  :   '"' ( ESC_SEQ | ~('\\'|'"') )* '"';
ESC_SEQ :       '\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\');
