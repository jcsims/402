// abstract base class for AST

import java.util.*;

public abstract class AST {
    private ArrayList<AST> children = new ArrayList<AST>();

    public final void addAST(AST child) {
	children.add(child);
    }

    public final AST getAST(int ix) {
	return children.get(ix);
    }

    public int size() {
	return children.size();
    }
}
