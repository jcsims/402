// implementation of rhsvar expression

import java.util.*;

public class VarExpr extends Expr {

    private String rhsvar;

    public VarExpr(String v) {
	rhsvar = v;
    }

    public String rhsVar() {
	return rhsvar;
    }
}
