/*******************************************************************************
 *  This is the toplevel driver program for the hw_5 pretty printer.
 *  For language details please see the grammar file 'hw_5.g'.
 *  This implementation uses a visitor to generate code
 *******************************************************************************/

import org.antlr.runtime.*;
import java.util.*;

public class PPrint {
    public static void main(String[] args) throws Exception {

	// check if we have an input file, if not print out error and abort
	if (args.length == 0) {
	    System.out.println("Usage: java PPrint <input file>");
	    System.exit(0);
	} else {
	    System.out.println("Processing: " + args[0]);
	}

	// set up and initialize our lexer and parser objects
	// open up the input file
        ANTLRFileStream input = new ANTLRFileStream(args[0]);
	// create the lexer with the input stream
        hw_5Lexer lexer = new hw_5Lexer(input);
	// create a token stream for the parser
        CommonTokenStream tokens = new CommonTokenStream(lexer);
	// create a parser object with the token stream
        hw_5Parser parser = new hw_5Parser(tokens);
	
	// call the toplevel recursive descent parsing function to construct
	// our IR
        AST ast = parser.prog();

	// pretty print our AST 
	PrettyPrintVisitor visitor = new PrettyPrintVisitor();
	visitor.dispatch(ast,0);
    }
}
