public class PrettyPrintVisitor {

    // public dispatcher for the pretty print visitor - we take
    // advantage of the reflective nature of Java and ask for the
    // class name of the current AST object and use the class name to
    // dispatch the correct pretty print behavior.
    public void dispatch(AST ast, int ix) {
	if      (ast.getClass() == AssignStmt.class) pprint((AssignStmt)ast,ix);
	else if (ast.getClass() == BlockStmt.class) pprint((BlockStmt)ast,ix);
	else if (ast.getClass() == GetStmt.class) pprint((GetStmt)ast,ix);
	else if (ast.getClass() == IfStmt.class) pprint((IfStmt)ast,ix);
	else if (ast.getClass() == PutStmt.class) pprint((PutStmt)ast,ix);
	else if (ast.getClass() == WhileStmt.class) pprint((WhileStmt)ast,ix);
	else if (ast.getClass() == StmtList.class) pprint((StmtList)ast,ix);
	else if (ast.getClass() == BinopExpr.class) pprint((BinopExpr)ast,ix);
	else if (ast.getClass() == NumExpr.class) pprint((NumExpr)ast,ix);
	else if (ast.getClass() == ParenExpr.class) pprint((ParenExpr)ast,ix);
	else if (ast.getClass() == VarExpr.class) pprint((VarExpr)ast,ix);
	else {
	    System.out.println("PrettyPrintVisitor: unknown class type");
	    System.exit(1);
	}
    }

    //****** pretty print statement level ASTs
    // pretty print statements
    private void pprint(AssignStmt ast, int ix) {
	printIndent(ix);

        System.out.print("(AssignStmt ");
	System.out.println(ast.lhsVar());
	this.dispatch(ast.getAST(0),ix + 1);
        System.out.print(")");
    }

    // pretty print block statements
    private void pprint(BlockStmt ast, int ix) {

	printIndent(ix);
	System.out.print("(BlockStmt ");

	// pretty print each of the statements
	for (int i = 0; i < ast.size(); i++) {
            System.out.println();
	    this.dispatch(ast.getAST(i),ix+1);
	}

	System.out.print(")");
    }

    // pretty print get statements
    private void pprint(GetStmt ast, int ix) {
	printIndent(ix);
	
	System.out.print("(GetStmt " + ast.lhsVar() + ")");
    }

    // pretty print if statements
    private void pprint(IfStmt ast, int ix) {
	printIndent(ix);
	System.out.println("(IfStmt ");
	// pretty print the expression
	this.dispatch(ast.getAST(0),ix+1);
        System.out.println();
	// pretty print the then clause
	this.dispatch(ast.getAST(1),ix+1);
	// pretty print else clause if it is there
	if (ast.size() == 3) {
	    AST elseClause = ast.getAST(2);
            System.out.println();
	    this.dispatch(elseClause,ix+1);
	}
        System.out.print(")");
    }

    // pretty print put statements
    private void pprint(PutStmt ast, int ix) {
	printIndent(ix);
	System.out.println("(PutStmt ");
       	this.dispatch(ast.getAST(0),ix+1);
	System.out.print(")");
    }

    // pretty print while statements
    private void pprint(WhileStmt ast, int ix) {
	printIndent(ix);
	System.out.println("(WhileStmt");
	// pretty print the expression
	this.dispatch(ast.getAST(0),ix + 1);
        System.out.println();
	// pretty print the while body
	this.dispatch(ast.getAST(1),ix + 1);
	System.out.print(")");
    }

    // pretty print statement lists
    private void pprint(StmtList ast, int ix) {
	printIndent(ix);

        System.out.print("(StmtList");
	// pretty print each of the statements
	for (int i = 0; i < ast.size(); i++) {
            System.out.println();
	    this.dispatch(ast.getAST(i),ix + 1);
	}
        System.out.print(")\n");

	printIndent(ix);
    }


    //****** pretty print expression level ASTs
    // pretty print binop expressions
    private void pprint(BinopExpr ast, int ix) {
        printIndent(ix);
	// print out op
        System.out.print("(BinopExpr");
	switch(ast.getOp()) {
	case BinopExpr.ADD:
	    System.out.println(" '+'");
	    break;
	case BinopExpr.MINUS:
	    System.out.println(" '-'");
	    break;
	case BinopExpr.MULT:
	    System.out.println(" '*'");
	    break;
	case BinopExpr.DIV:
	    System.out.println(" '/'");
	    break;
	case BinopExpr.EQ:
	    System.out.println(" '==' ");
	    break;
	case BinopExpr.LESSEQ:
	    System.out.println(" '<='");
	    break;
	default:
	    System.out.print("Pretty Print Visitor: unknown binary");
	    System.out.print("operator expression.\n");
	    System.exit(1);
	}
	// pretty print left child
	this.dispatch(ast.getAST(0),ix + 1);
        System.out.println();
	// pretty print right child
	this.dispatch(ast.getAST(1),ix + 1);
        System.out.print(")");
    }

    // pretty print number expressions
    private void pprint(NumExpr ast, int ix) {
        printIndent(ix);
	System.out.print("(NumExpr " + ast.getValue()+ ")");
    }

    // pretty print parenthesized expressions
    private  void pprint(ParenExpr ast, int ix) {
        printIndent(ix);
	System.out.println("(ParenExpr");
	this.dispatch(ast.getAST(0),ix + 1);
	System.out.print(")");
    }

    // pretty print rhs variable expressions
    private void pprint(VarExpr ast, int ix) {
        printIndent(ix);
	System.out.print("(VarExpr " + ast.rhsVar() + ")");
    }

    // print indentation
    private void printIndent(int r) {
	for (int i = 0; i < r; i++)
	    System.out.print("  ");
    }
}
