// implementation of formal parameter


public class FormalVarExpr extends Expr {

    private int type;
    private String name;

    public FormalVarExpr(int t,String v) {
	type = t;
	name = v;
    }

    public int getType() {
	return type;
    }

    public String getVarName() {
	return name;
    }
}
