// FloatConst value

class FloatConst extends Value {
    
    private Float value;

    public FloatConst(String value) {
	this.value=new Float(Float.parseFloat(value));
    }

    public FloatConst(Float value) {
	this.value=value;
    }

    public Float getValue() {
	return value;
    }

    public int getType() {
	return Value.FLOAT;
    }

    public String toString() {
	return value.toString();
    }
}