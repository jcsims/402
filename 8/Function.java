// function value

public class Function extends Value {

    private int returnType;
    private ArgList formalParameters;
    private Stmt functionBody;

    // we are implementing static scoping, so once
    // we declare a function during interpretation we
    // need to keep track of the scope were it was declared.
    private SymbolTableScope parentScope = null;

    public Function(int returnType,ArgList formalParameters, Stmt body) {
	this.returnType=returnType;
	this.formalParameters=formalParameters;
	this.functionBody=body;
    }

    public String toString() {
	return "<function value>";
    }

    public int getType() {
	return Value.FUNCTION;
    }

    public int getReturnType() {
	return returnType;
    }

    public ArgList getFormalParameters() {
	return formalParameters;
    }

    public Stmt getFunctionBody() {
	return functionBody;
    }

    public void setParentScope (SymbolTableScope parentScope) {
	this.parentScope = parentScope;
    }

    public SymbolTableScope getParentScope () {
	return this.parentScope;
    }
}
