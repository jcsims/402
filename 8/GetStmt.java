// get statement

public class GetStmt extends Stmt {

    private String prompt;
    private String var;

    public GetStmt(String p,String v) {
	prompt = p;
	var = v;
    }

    public String getPrompt() {
	return prompt;
    }

    public String getVar() {
	return var;
    }
}