// StringConst value

class StringConst extends Value {
    
    private String value;

    public StringConst(String value) {
	this.value=value;
    }

    public String getValue() {
	return value;
    }

    public int getType() {
	return Value.STRING;
    }

    public String toString() {
	return value;
    }
}