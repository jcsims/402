// base class for values

abstract class Value {
    public static final int NOTYPE = -1;
    public static final int INTEGER = 0;
    public static final int FLOAT = 1;
    public static final int STRING = 2;
    public static final int FUNCTION = 3;

    // This table implements ints as a subtype of floats
    // and floats as a subtype of strings
    private static int[][] typeArray = {
	// INTEGER   FLOAT    STRING    FUNCTION
	//--------------------------------------------
	{  INTEGER,  FLOAT,   STRING,   NOTYPE },   // INTEGER
	{  FLOAT,    FLOAT,   STRING,   NOTYPE },   // FLOAT
	{  STRING,   STRING,  STRING,   NOTYPE },   // STRING
	{  NOTYPE,   NOTYPE,  NOTYPE,   NOTYPE }    // FUNCTION
    }; 

    public static int getResultType(int lt,int rt) {
	if (lt == NOTYPE || rt == NOTYPE)
	    return NOTYPE;
	else
	    return typeArray[lt][rt];
    }

    // every derived class needs to implement the following behavior
    public abstract int getType();
    public abstract String toString();
}
