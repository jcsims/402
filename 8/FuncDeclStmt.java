// function declaration statement

public class FuncDeclStmt extends Stmt {

    private String name;
    private Function value;

    public FuncDeclStmt(String name, Function f) {
	this.name = name;
	this.value = f;
    }

    public String getName() {
	return name;
    }

    public Function getValue() {
	return value;
    }
}