
public class SymbolTable {

    private SymbolTableScope globalScope = new SymbolTableScope(null);
    private SymbolTableScope currentScope = globalScope;

    public SymbolTableScope getCurrentScope() {
	return this.currentScope;
    }

    public void setCurrentScope(SymbolTableScope scope) {
	this.currentScope = scope;
    }

    public SymbolTableScope  pushScope() {
	currentScope = new SymbolTableScope(currentScope);
	// we need to carry the function value of the surrounding
	// function with us so that we can typecheck the return
	// value properly
	currentScope.setFunctionValue(currentScope.getParentScope().getFunctionValue());
	return currentScope;
    }

    public SymbolTableScope popScope() {
	// go up one entry 
	currentScope = currentScope.getParentScope();
	return currentScope;
    }

    public void declareSymbol(String symbol, Value value) {
	// check that the current symbol was not already declared in the
	// current scope, if so then we have an error
	if (currentScope.lookupSymbol(symbol) != null) {
	    System.err.println("Error: redclaring symbol "+symbol+".");
	    System.exit(1);
	}
	// all clear...enter the symbol into the scope
	currentScope.enterSymbol(symbol,value);
    }

    public SymbolTableScope findScope(String symbol) {
	// find the scope of a symbol declaration

	// first lookup the symbol in the current scope
	SymbolTableScope lookupScope = currentScope;
	Value value = lookupScope.lookupSymbol(symbol);

	// if not in current scope search up the stack
	while (value == null) {
	    lookupScope = lookupScope.getParentScope();
	    if (lookupScope == null) {
		// no parent scope, symbol not found
		System.err.println("Error (lookup): symbol '"+symbol+"' not declared.");
		System.exit(1);
		return null;
	    }
	    value = lookupScope.lookupSymbol(symbol);
	}

	return lookupScope;
    }

    public Value lookupSymbol(String symbol) {
	// find the scope of declaration
	SymbolTableScope lookupScope = findScope(symbol);

	// all done, return the value, guaranteed to be here
	// by the nature of our search procedure
	return lookupScope.lookupSymbol(symbol);
    }

    public void updateVariable(String symbol, Value initValue) {
	// find the scope where the symbol was declared
	SymbolTableScope lookupScope = findScope(symbol);

	// we found a scope where symbol is defined, update it
	// make sure that it is a variable
	if (lookupScope.lookupSymbol(symbol).getType() != Value.FUNCTION) {
	    lookupScope.enterSymbol(symbol,initValue);
	}
	else {
		System.err.println("Error (update): symbol '"+symbol+"' is not a variable.");
		System.exit(1);
	}
    }
}