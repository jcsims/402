// this visitor class implements the simple4 interpreter.

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class InterpVisitor {

    // reading the console input
    private InputStreamReader converter = new InputStreamReader(System.in);
    private BufferedReader consoleInput = new BufferedReader(converter);
    
    // initializing function parameters in the current scope
    // this function implements positional parameter correspondence.
    private void initParameters(ArgList formalParameters, ArgList actualParameters) 
                                throws ReturnValueException {

	// first check that the list sizes are the same
	if (formalParameters.size() != actualParameters.size()) {
	    System.err.println("Error (initParameters): formal and actual parameter lists do not match.");
	    System.exit(1);
	}

	// now simply step through the lists and declare the formal parameters as local variables
	for (int i = 0; i < formalParameters.size(); i++) {
	    // evaluate the actual expression
	    Value value = this.dispatch(actualParameters.getAST(i));
	    // get the formal parameter name
	    FormalVarExpr var = (FormalVarExpr) formalParameters.getAST(i);
	    String name = var.getVarName();
	    // declare the variable with its init value
	    Interpret.symbolTable.declareSymbol(name,value);
	}
    }

    // the dispatcher for the interpreter visitor
    public Value dispatch(AST ast) throws ReturnValueException {
	if      (ast.getClass() == AssignStmt.class) return interp((AssignStmt)ast);
	else if (ast.getClass() == BlockStmt.class) return interp((BlockStmt)ast);
	else if (ast.getClass() == GetStmt.class) return interp((GetStmt)ast);
	else if (ast.getClass() == IfStmt.class) return interp((IfStmt)ast);
	else if (ast.getClass() == PutStmt.class) return interp((PutStmt)ast);
	else if (ast.getClass() == WhileStmt.class) return interp((WhileStmt)ast);
	else if (ast.getClass() == StmtList.class) return interp((StmtList)ast);
	else if (ast.getClass() == BinopExpr.class) return interp((BinopExpr)ast);
	else if (ast.getClass() == ConstExpr.class) return interp((ConstExpr)ast);
	else if (ast.getClass() == ParenExpr.class) return interp((ParenExpr)ast);
	else if (ast.getClass() == VarExpr.class) return interp((VarExpr)ast);
	else if (ast.getClass() == FuncDeclStmt.class) return interp((FuncDeclStmt)ast);
	else if (ast.getClass() == VarDeclStmt.class) return interp((VarDeclStmt)ast);
	else if (ast.getClass() == CallStmt.class) return interp((CallStmt)ast);
	else if (ast.getClass() == CallExpr.class) return interp((CallExpr)ast);
	else if (ast.getClass() == ReturnStmt.class) return interp((ReturnStmt)ast);
	else if (ast.getClass() == CastExpr.class) return interp((CastExpr)ast);
	else {
	    System.out.println("Error (InterpVisitor): unknown class type");
	    System.exit(1);
	    return null;
	}
    }

    //****** interpret statement level ASTs
    // assignment statements
    private Value interp(AssignStmt ast) throws ReturnValueException {

	// evaluate the expression
	Value value = this.dispatch(ast.getAST(0));
	// assign the value to the lhs var
	Interpret.symbolTable.updateVariable(ast.lhsVar(),value);

	// statements do not have return values -- null
	return null;
    }

    // block statements
    private Value interp(BlockStmt ast) throws ReturnValueException {
	// set up the scope for this block
	Interpret.symbolTable.pushScope();

	// interpret each of the statements in the block
	for (int i = 0; i < ast.size(); i++) {
	    this.dispatch(ast.getAST(i));
	}

	// leaving this scope -- set current scope to parent scope
	Interpret.symbolTable.popScope();

	// statements do not have return values -- null
	return null;
    }

    // get statements
    private Value interp(GetStmt ast) {

	String prompt = ast.getPrompt() == ""?"Enter a value: ":ast.getPrompt();

	try {
	    // get the value string from the user
	    System.out.print(prompt);
	    String inputString = consoleInput.readLine();

	    // now look at the variable in the symbol table 
	    // in order decide how to parse the input string
	    int varType = Interpret.symbolTable.lookupSymbol(ast.getVar()).getType();

	    // parse the input string
	    Value value;
	    switch(varType) {
	    case Value.INTEGER:
		value = new IntConst(inputString);
		break;
	    case Value.FLOAT:
		value = new FloatConst(inputString);
		break;
	    case Value.STRING:
		value = new StringConst(inputString);
		break;
	    default:
		System.err.println("Error (getstmt): input type unknown.");
		System.exit(1);
		return null;
	    }

	    // assign the value to the lhs var
	    Interpret.symbolTable.updateVariable(ast.getVar(),value);

	    // statements do not have return values -- null
	    return null;
	}
	catch(Exception e) {
	    System.out.println("Interpreter Visitor: exception " + e);
	    System.exit(1);
	    return null;
	}
    }

    // if statements
    private Value interp(IfStmt ast) throws ReturnValueException {
	// interpret the expression
	// NOTE: we are guaranteed that this is an IntConst by the typechecker
	IntConst value = (IntConst) this.dispatch(ast.getAST(0));

	if (value.getValue().intValue() != 0) {
	    // interpret the then clause
	    this.dispatch(ast.getAST(1));
	}
	else if (ast.size() == 3) {
	    // interpret the else clause if we have one
	    this.dispatch(ast.getAST(2));
	}

	// statements do not have return values -- null
	return null;
    }

    // put statements
    private Value interp(PutStmt ast) throws ReturnValueException {
	// interpret the expression
       	ArgList list = (ArgList)ast.getAST(0);

	for(int i=0; i < list.size(); i++) {
	    Value value = this.dispatch(list.getAST(i));
	    System.out.print(value.toString());
	}

	System.out.println();

	// statements do not have return values -- null
	return null;
    }

    // while statements
    private Value interp(WhileStmt ast) throws ReturnValueException {
	IntConst value;

	// interpet the expression
	// NOTE: typechecker guarantees that this is an integer
	value = (IntConst)this.dispatch(ast.getAST(0));

	// interpret the loop while the expression value != 0
	while (value.getValue().intValue() != 0) {
	    // interpret the while body
	    this.dispatch(ast.getAST(1));
	    // reevaluate the loop expression
	    value = (IntConst)this.dispatch(ast.getAST(0));
	}

	// statements do not have return values -- null
	return null;
    }

    // statement lists
    private Value interp(StmtList ast) throws ReturnValueException {

	// interpret each of the statements
	for (int i = 0; i < ast.size(); i++) {
	    this.dispatch(ast.getAST(i));
	}

	// statements do not have return values -- null
	return null;
    }


    // function declaration statements
    private Value interp(FuncDeclStmt ast) {

	// we are implementing static scoping, so we
	// have to insert the current scope as the parent
	// scope for this function declaration
	Function fValue = ast.getValue();
	fValue.setParentScope(Interpret.symbolTable.getCurrentScope());

	// declare the function
	Interpret.symbolTable.declareSymbol(ast.getName(),fValue);

	// statements do not have return values -- null
	return null;
    }

    // variable declaration statements
    private Value interp(VarDeclStmt ast) throws ReturnValueException {

	// evaluate the init expression
	Value value = this.dispatch(ast.getAST(0));
	// declare the variable with its init value
	Interpret.symbolTable.declareSymbol(ast.getVar(),value);

	// statements do not have return values -- null
	return null;
    }

    // call statements -- function calls as statements -- ignore the return value
    private Value interp(CallStmt ast) throws ReturnValueException {

	// lookup the function value of the function name
	Function fValue = (Function)Interpret.symbolTable.lookupSymbol(ast.getFunctionName());
	
	// implement static scoping
	// save the current top of stack
	SymbolTableScope topOfStack = Interpret.symbolTable.getCurrentScope();
	// push function local scope onto the stack
	Interpret.symbolTable.pushScope();
	// Initialize formal parameters with actual parameters in the current scope
	initParameters(fValue.getFormalParameters(),ast.getActualParameters());
	// set the parent of the current scope to be the parent scope of the function
	Interpret.symbolTable.getCurrentScope().setParentScope(fValue.getParentScope());


	// execute the body of the function
	try {
	    this.dispatch(fValue.getFunctionBody());
	}
	catch (ReturnValueException e) {
	    // ignore!
	}

	// pop the function scope off the stack
	Interpret.symbolTable.popScope();
	// we are now in the parent scope of the function --
	// we have to restore the original scope on the top of the stack
	Interpret.symbolTable.setCurrentScope(topOfStack);

	// statements do not have return values -- null
	return null;
    }

    // return statements
    private Value interp(ReturnStmt ast) throws ReturnValueException {

	if (ast.size() == 0) {
	    // no return value express, throw a null
	    // return value object.
	    throw new ReturnValueException(null);
	}
	else {
	    // evaluate the return expression
	    Value returnValue = this.dispatch(ast.getExpr());

	    // now throw the return value object
	    throw new ReturnValueException(returnValue);
	}
    }

    //****** interpret expression level ASTs
    // binop expressions
    private Value interp(BinopExpr ast) throws ReturnValueException {

	// interpret left child
	Value left = this.dispatch(ast.getAST(0));
	// interpret right child
	Value right = this.dispatch(ast.getAST(1));

	// NOTE: the typechecker guarantees that both
	// operands have the same type
	if (left.getType() == Value.INTEGER) {
	    int lv = ((IntConst)left).getValue().intValue();
	    int rv = ((IntConst)right).getValue().intValue();
	    // compute the return value based on the OP
	    switch(ast.getOp()) {
	    case BinopExpr.ADD:
		return new IntConst(lv + rv);
	    case BinopExpr.MINUS:
		return new IntConst(lv - rv);
	    case BinopExpr.MULT:
		return new IntConst(lv * rv);
	    case BinopExpr.DIV:
		return new IntConst(lv / rv);
	    case BinopExpr.EQ:
		return new IntConst((lv == rv)?1:0);
	    case BinopExpr.LESSEQ:
		return new IntConst((lv <= rv)?1:0);
	    default:
		System.out.println("Error (InterpVisitor): unknown binary operator expression.");
		System.exit(1);
		return null;
	    }
	}
	else if (left.getType() == Value.FLOAT) {
	    float lv = ((FloatConst)left).getValue().floatValue();
	    float rv = ((FloatConst)right).getValue().floatValue();
	    // compute the return value based on the OP
	    switch(ast.getOp()) {
	    case BinopExpr.ADD:
		return new FloatConst(lv + rv);
	    case BinopExpr.MINUS:
		return new FloatConst(lv - rv);
	    case BinopExpr.MULT:
		return new FloatConst(lv * rv);
	    case BinopExpr.DIV:
		return new FloatConst(lv / rv);
	    case BinopExpr.EQ:
		return new IntConst((lv == rv)?1:0);
	    case BinopExpr.LESSEQ:
		return new IntConst((lv <= rv)?1:0);
	    default:
		System.out.println("Error (InterpVisitor): unknown binary operator expression.");
		System.exit(1);
		return null;
	    }
	}
	else if (left.getType() == Value.STRING) {
	    String lv = ((StringConst)left).getValue();
	    String rv = ((StringConst)right).getValue();
	    // compute the return value based on the OP
	    switch(ast.getOp()) {
	    case BinopExpr.ADD:
		return new StringConst(lv + rv);
	    case BinopExpr.EQ:
		return new IntConst((lv.equals(rv))?1:0);
	    default:
		System.out.println("Error (InterpVisitor): unknown binary operator expression.");
		System.exit(1);
		return null;
	    }
	}
	else {
	    System.out.println("Error (InterpVisitor): unknown binary operator type.");
	    System.exit(1);
	    return null;
	}
    }

    // number expressions
    private Value interp(ConstExpr ast) {
	return ast.getValue();
    }

    // parenthesized expressions
    private  Value interp(ParenExpr ast) throws ReturnValueException {
	return this.dispatch(ast.getAST(0));
    }

    // rhs variable expressions
    private Value interp(VarExpr ast) {
	// fetch the variable value from symbol table
	return Interpret.symbolTable.lookupSymbol(ast.getVarName());
    }

    // call expressions - call to functions within expressions -- have
    // deal with the return value.
    private Value interp(CallExpr ast) throws ReturnValueException {

	// lookup the function value of the function name
	Function fValue = (Function)Interpret.symbolTable.lookupSymbol(ast.getFunctionName());
	
	// implement static scoping
	// save the current top of stack
	SymbolTableScope topOfStack = Interpret.symbolTable.getCurrentScope();
	// push function local scope onto the stack
	Interpret.symbolTable.pushScope();
	// Initialize formal parameters with actual parameters in the current scope
	initParameters(fValue.getFormalParameters(),ast.getActualParameters());
	// the the parent of the current scope to be the parent scope of the function
	Interpret.symbolTable.getCurrentScope().setParentScope(fValue.getParentScope());

	// execute the body of the function and retrieve the return value
	Value returnValue = null;
	try {
	    this.dispatch(fValue.getFunctionBody());
	    // if we got here then we did not receive a return value exception -- error!
	    System.err.println("Error: expected return value from function '"+ast.getFunctionName()+"'.");
	    System.exit(1);
	}
	catch (ReturnValueException e) {
	    returnValue = e.getReturnValue();

	    // if this is null then we saw a return without a value
	    if (returnValue == null) {
		System.err.println("Error: expected return value from function '"+ast.getFunctionName()+"'.");
		System.exit(1);
	    }
	}

	// pop the function scope off the stack
	Interpret.symbolTable.popScope();
	// we are now in the parent scope of the function --
	// we have to restore the original scope on the top of the stack
	Interpret.symbolTable.setCurrentScope(topOfStack);

	// the return value of a function call is the value of the 
	// called function
	return returnValue;
    }

    // cast expression
    private Value interp(CastExpr ast) throws ReturnValueException {
	int to = ast.getToType();

	Value value = this.dispatch(ast.getAST(0));

	switch (to) {
	case Value.FLOAT:
	    return new FloatConst(value.toString());
	case Value.STRING:
	    return new StringConst(value.toString());
	default:
	    System.err.println("Error (castexpr): target type not recognized.");
	    System.exit(1);
	    return null;
	}
    }
}