// IntConst value

class IntConst extends Value {
    
    private Integer value;

    public IntConst(String value) {
	this.value=new Integer(Integer.parseInt(value));
    }

    public IntConst(Integer value) {
	this.value=value;
    }

    public Integer getValue() {
	return value;
    }

    public int getType() {
	return Value.INTEGER;
    }

    public String toString() {
	return value.toString();
    }
}