
import java.util.*;

public class SymbolTableScope {

    // scope stack is built as a linked list
    private SymbolTableScope parentScope = null;

    // function value, if this is the scope of a function call
    // otherwise null
    Function function = null;

    // symbols are kept in a hashmap indexed by their name
    // their initialization value depends on their kind:
    //    integer,float, and string constants
    // and
    //    function values
    //
    private HashMap<String,Value> value = new HashMap<String,Value>();

    public SymbolTableScope(SymbolTableScope parentScope) {
	this.parentScope = parentScope;
    }

    public SymbolTableScope getParentScope() {
	return parentScope;
    }

    public void setParentScope(SymbolTableScope parentScope) {
	this.parentScope = parentScope;
    }

    public void enterSymbol(String name, Value value) {
	this.value.put(name,value);
    }

    public Value lookupSymbol(String name) {
	return value.get(name);
    }

    public void setFunctionValue(Function value) {
	function = value;
    }

    public Function getFunctionValue() {
	return function;
    }
}
