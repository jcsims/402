grammar simple4;

//********************************************************
// same as simple3 but now includes data types.
//
// NOTE: we also require ';' terminated statements.
// this is mainly due to the "empty" return statement
// which would be ambiguous without the semicolon.

// grammar rules

options{
k=4;
}

@members{
// override the default error reporting functions
public void reportError(RecognitionException e) {
    // call the Parser member function to report the error
    displayRecognitionError(this.getTokenNames(), e);
    // exit with error
    System.exit(1);
}
}

prog returns [AST ast]
	:	{$ast = new StmtList();} (stmt {$ast.addAST($stmt.ast);})+
	;

stmt returns [Stmt ast]
	:	dt=dataType VAR '(' ')' s=stmt			{ $ast = new FuncDeclStmt($VAR.text,new Function($dt.type,new ArgList(),$s.ast));	}
	|	dt=dataType VAR '(' l=formalParamList ')' s=stmt	{ $ast = new FuncDeclStmt($VAR.text,new Function($dt.type,$l.ast,$s.ast)); }
	|	dt=dataType VAR '=' exp ';'			{ $ast = new VarDeclStmt($dt.type,$VAR.text,$exp.ast); }
	|	dt=dataType VAR ';'				{ $ast = new VarDeclStmt($dt.type,$VAR.text,new ConstExpr(new IntConst("0"))); }
	|	VAR '=' exp ';'					{ $ast = new AssignStmt($VAR.text,$exp.ast); }
	|	'get' prompt ',' VAR ';'			{ $ast = new GetStmt($prompt.text,$VAR.text); }
	|	'get'  VAR ';'					{ $ast = new GetStmt("",$VAR.text); }
	|	'put' argList ';'				{ $ast = new PutStmt($argList.ast); }
	|	VAR '(' l=actualParamList ')' ';'		{ $ast = new CallStmt($VAR.text,$l.ast);}
	|	VAR '(' ')' ';'					{ $ast = new CallStmt($VAR.text);}
	|	'return' exp ';'				{ $ast = new ReturnStmt($exp.ast); }
	|	'return' ';'					{ $ast = new ReturnStmt(); }
	|	'while' '(' exp ')' s=stmt			{ $ast = new WhileStmt($exp.ast,$s.ast); }
	|	'if' '(' exp ')' s1=stmt {$ast = new IfStmt($exp.ast,$s1.ast);}('else' s2=stmt {$ast.addAST($s2.ast);})?  
	|	'{' {$ast = new BlockStmt();} (s=stmt {$ast.addAST($s.ast);})+ '}'
	;

dataType returns [int type]
	:	'int'		{$type=Value.INTEGER;}
	|	'float'		{$type=Value.FLOAT;}
	|	'string'	{$type=Value.STRING;}
	;

prompt returns [String text]
	:	string		{$text=$string.text;}
	;
		
formalParamList returns [ArgList ast]
	:	d1=dataType v1=VAR {$ast = new ArgList(new FormalVarExpr($d1.type,$v1.text));}(',' d2=dataType v2=VAR {$ast.addAST(new FormalVarExpr($d2.type,$v2.text));} )*
	;
	
actualParamList returns [ArgList ast]
	:	e1= exp {$ast = new ArgList($e1.ast);} (',' e2=exp {$ast.addAST($e2.ast);} )*
	;

argList returns [ArgList ast]
	:	e1= exp {$ast = new ArgList($e1.ast);} (',' e2=exp {$ast.addAST($e2.ast);} )*
	;
	
exp returns [Expr ast]
	:	relexp {$ast = $relexp.ast; };

relexp returns [Expr ast]
	:	e1=addexp { $ast = $e1.ast; } 
		(
			('==' e2=addexp { $ast = new BinopExpr(BinopExpr.EQ,$ast,$e2.ast); })|
			('<=' e3=addexp { $ast = new BinopExpr(BinopExpr.LESSEQ,$ast,$e3.ast); })
		)*
	;

addexp returns [Expr ast]
	:	e1=mulexp { $ast = $e1.ast; }
		(
			('+' e2=mulexp { $ast = new BinopExpr(BinopExpr.ADD,$ast,$e2.ast); })|
			('-' e3=mulexp { $ast = new BinopExpr(BinopExpr.MINUS,$ast,$e3.ast); })
		)* 
	;

mulexp returns [Expr ast]
	:	e1=atom { $ast = $e1.ast; }
		(
			('*' e2=atom { $ast = new BinopExpr(BinopExpr.MULT,$ast,$e2.ast); })|
			('/' e3=atom { $ast = new BinopExpr(BinopExpr.DIV,$ast,$e3.ast); })
		)* 
	;

atom returns [Expr ast]
	:	'(' exp ')'   			{ $ast = new ParenExpr($exp.ast); }
	|	VAR '(' l=actualParamList ')' 	{ $ast = new CallExpr($VAR.text,$l.ast);}
	|	VAR '(' ')'			{ $ast = new CallExpr($VAR.text);}
	|	VAR				{ $ast = new VarExpr($VAR.text); }
	|	'-' INT				{ $ast = new ConstExpr(new IntConst('-'+$INT.text)); }
	|	INT				{ $ast = new ConstExpr(new IntConst($INT.text)); }
	|	'-' FLOAT			{ $ast = new ConstExpr(new FloatConst('-'+$FLOAT.text)); }
	|	FLOAT				{ $ast = new ConstExpr(new FloatConst($FLOAT.text)); }
	|	string				{ $ast = new ConstExpr(new StringConst($string.text)); }
	;

string returns [String text]
	:	STRING	
		   {
		      // get rid of the quotes in the string
		      int size = $STRING.text.length();
		      $text = $STRING.text.substring(1,size-1);
		   } 
	;

//*************************************************************************
// lexical analyzer stuff
// NOTE: putting negative number recognition into the lexer does
// not work, hides the minus sing from the parser and this leads
// to syntax error, need to expose the minus sign at the rule level, see above
VAR  	:	('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')* ;
STRING	:  	'"' ( ESC_SEQ | ~('\\'|'"') )* '"';
INT 	:	'0'..'9'+;
FLOAT   :   	('0'..'9')+ '.' ('0'..'9')* EXPONENT?
    	|   	'.' ('0'..'9')+ EXPONENT?
  	|   	('0'..'9')+ EXPONENT
  	;

fragment
ESC_SEQ	:   	'\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\');

fragment
EXPONENT : ('e'|'E') ('+'|'-')? ('0'..'9')+ ;

COMMENT	:   	'//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;};
WS  	:   	( ' ' | '\t' | '\r' | '\n' ) {$channel=HIDDEN;};

