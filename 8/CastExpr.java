// a cast expression promotes an expression from one type to another
// as long as they are related by a subtype supertype relationship

public class CastExpr extends Expr {
    private int fromType;
    private int toType;

    public CastExpr(int from,int to,Expr e) {
	fromType = from;
	toType = to;
	this.addAST(e);
    }

    public int getFromType() {
	return fromType;
    }

    public int getToType() {
	return toType;
    }
}
