// this visitor class implements the simple4 static type checker.


public class TypeCheckVisitor {

    // typecheck function parameters
    private void typecheckParameters(ArgList formalParameters, ArgList actualParameters) {
	// first check that the list sizes are the same
	if (formalParameters.size() != actualParameters.size()) {
	    System.err.println("Error (typecheckParameters): formal and actual parameter lists do not match.");
	    System.exit(1);
	}

	// now simply step through the lists and declare the formal parameters as local variables
	for (int i = 0; i < formalParameters.size(); i++) {
	    // evaluate the actual expression
	    int exprType = this.dispatch(actualParameters.getAST(i));
	    // get the formal parameter name
	    FormalVarExpr var = (FormalVarExpr) formalParameters.getAST(i);
	    int varType = var.getType();

	    // check if this makes sense from a type perspective
	    int resultType = Value.getResultType(varType,exprType);

	    // check for type errors
	    if (resultType == Value.NOTYPE || resultType != varType) {
		System.err.println("Error (TypecheckParameters): expression type "+resultType+" cannot be assigned to variable of type "+varType);
		System.exit(1);
	    }

	    // check if we have to insert a type promotion
	    if (resultType != exprType) {
		AST newAst = new CastExpr(exprType,resultType,(Expr)actualParameters.getAST(i));
		actualParameters.putAST(0,newAst);
	    }
	}
    }

    // initializing function parameters in the current scope
    // this function implements positional parameter correspondence.
    // this function is only used for typechecking purposes, it initializes all
    // formal parameters to zero.
    private void declareParameters(ArgList formalParameters) {
	// now simply step through the list and declare the formal parameters as local variables
	for (int i = 0; i < formalParameters.size(); i++) {
	    // get the formal parameter name
	    FormalVarExpr var = (FormalVarExpr) formalParameters.getAST(i);
	    String name = var.getVarName();
	    int type = var.getType();
	    Value value;

	    switch(type) {
	    case Value.INTEGER:
		value = new IntConst(new Integer(0));
		break;
	    case Value.FLOAT:
		value = new FloatConst(new Float(0.0));
		break;
	    case Value.STRING:
		value = new StringConst("");
		break;
	    default:
		System.err.println("Error: unknown type in declareParameters.");
		System.exit(1);
		value = null;
	    }

	    // declare the variable with its init value
	    Interpret.symbolTable.declareSymbol(name,value);
	}
    }

    // the dispatcher for the type check visitor
    public int dispatch(AST ast) {
	if      (ast.getClass() == AssignStmt.class) return interp((AssignStmt)ast);
	else if (ast.getClass() == BlockStmt.class) return interp((BlockStmt)ast);
	else if (ast.getClass() == GetStmt.class) return interp((GetStmt)ast);
	else if (ast.getClass() == IfStmt.class) return interp((IfStmt)ast);
	else if (ast.getClass() == PutStmt.class) return interp((PutStmt)ast);
	else if (ast.getClass() == WhileStmt.class) return interp((WhileStmt)ast);
	else if (ast.getClass() == StmtList.class) return interp((StmtList)ast);
	else if (ast.getClass() == BinopExpr.class) return interp((BinopExpr)ast);
	else if (ast.getClass() == ConstExpr.class) return interp((ConstExpr)ast);
	else if (ast.getClass() == ParenExpr.class) return interp((ParenExpr)ast);
	else if (ast.getClass() == VarExpr.class) return interp((VarExpr)ast);
	else if (ast.getClass() == FuncDeclStmt.class) return interp((FuncDeclStmt)ast);
	else if (ast.getClass() == VarDeclStmt.class) return interp((VarDeclStmt)ast);
	else if (ast.getClass() == CallStmt.class) return interp((CallStmt)ast);
	else if (ast.getClass() == CallExpr.class) return interp((CallExpr)ast);
	else if (ast.getClass() == ReturnStmt.class) return interp((ReturnStmt)ast);
	else {
	    System.err.println("Error (InterpVisitor): unknown class type");
	    System.exit(1);
	    return Value.NOTYPE;
	}
    }

    //****** type check statement level ASTs
    // assignment statements
    private int interp(AssignStmt ast) {

	// typecheck the expression
	int exprType = this.dispatch(ast.getAST(0));
	// get the type of the variable
	int varType = Interpret.symbolTable.lookupSymbol(ast.lhsVar()).getType();

	// types compatible?
        int resultType = Value.getResultType(varType,exprType);

	// check for type errors
	if (resultType == Value.NOTYPE || 
	    resultType != varType) {  // second condition means: assigning supertype to subtype
	    System.err.println("Error (assignmentstmt): expression type "+resultType+" cannot be assigned to variable of type "+varType);
	    System.exit(1);
	    return Value.NOTYPE;
	}

	// check if we have to insert a type promotion
	if (resultType != exprType) {
	    AST newAst = new CastExpr(exprType,resultType,(Expr)ast.getAST(0));
	    ast.putAST(0,newAst);
	}

	// statements do not have types
	return Value.NOTYPE;
    }

    // block statements
    private int interp(BlockStmt ast) {
	// set up the scope for this block
	Interpret.symbolTable.pushScope();

	// interpret each of the statements in the block
	for (int i = 0; i < ast.size(); i++) {
	    this.dispatch(ast.getAST(i));
	}

	// leaving this scope -- set current scope to parent scope
	Interpret.symbolTable.popScope();

	// statements do not have types
	return Value.NOTYPE;
    }

    // get statements
    private int interp(GetStmt ast) {
	// get the type of the variable...we cannot get new functions
	int symbolType =Interpret.symbolTable.lookupSymbol(ast.getVar()).getType();

	if (symbolType == Value.FUNCTION) {
	    System.err.println("Error: cannot read in function values.");
	    System.exit(1);
	    return Value.NOTYPE;
	}

	return Value.NOTYPE;
    }

    // if statements
    private int interp(IfStmt ast) {
	// typecheck the expression -- has to be an integer
	int exprType = this.dispatch(ast.getAST(0));

	if (exprType != Value.INTEGER) {
	    System.err.println("Error: expression of an if-stmt has to be of type integer.");
	    System.exit(1);
	    return Value.NOTYPE;
	}

	// type check the then clause
	this.dispatch(ast.getAST(1));

	// typecheck the else clause if we have one	
	if (ast.size() == 3) {
	    this.dispatch(ast.getAST(2));
	}

	// statements do not have return values -- null
	return Value.NOTYPE;
    }

    // put statements
    private int interp(PutStmt ast) {
	// walk the expression list and make sure that
	// there are no functions on the list.
       	ArgList list = (ArgList)ast.getAST(0);

	for(int i=0; i < list.size(); i++) {
	    int exprType = this.dispatch(list.getAST(i));

	    if (exprType == Value.FUNCTION) {
		System.err.println("Error: a function appears in the put expression list.");
		System.exit(1);
		return Value.NOTYPE;
	    }
	}

	// statements do not have return values -- null
	return Value.NOTYPE;
    }

    // while statements
    private Integer interp(WhileStmt ast) {
	// typecheck the expression -- has to be an integer
	int exprType = this.dispatch(ast.getAST(0));

	if (exprType != Value.INTEGER) {
	    System.err.println("Error: expression of a while-stmt has to be of type integer.");
	    System.exit(1);
	    return Value.NOTYPE;
	}

	// type check the body of the loop
	this.dispatch(ast.getAST(1));

	// statements do not have types
	return Value.NOTYPE;
    }

    // statement lists
    private int interp(StmtList ast) {

	// interpret each of the statements
	for (int i = 0; i < ast.size(); i++) {
	    this.dispatch(ast.getAST(i));
	}

	// statements do not have types
	return Value.NOTYPE;
    }


    // function declaration statements
    private Integer interp(FuncDeclStmt ast) {

	// we are implementing static scoping, so we
	// have to insert the current scope as the parent
	// scope for this function declaration
	Function fValue = ast.getValue();
	fValue.setParentScope(Interpret.symbolTable.getCurrentScope());

	// declare the function
	Interpret.symbolTable.declareSymbol(ast.getName(),fValue);

	// now type check the function
	// implement static scoping
	// save the current top of stack
	SymbolTableScope topOfStack = Interpret.symbolTable.getCurrentScope();

	// push function local scope onto the stack
	Interpret.symbolTable.pushScope();

	// remember the function value to which this particular scope object belongs to;
	// is needed to type check the return statement.
	Interpret.symbolTable.getCurrentScope().setFunctionValue(fValue);

	// Initialize formal parameters with actual parameters in the current scope
	declareParameters(fValue.getFormalParameters());

	// set the parent of the current scope to be the parent scope of the function
	Interpret.symbolTable.getCurrentScope().setParentScope(fValue.getParentScope());


	// type check the body of the function
	this.dispatch(fValue.getFunctionBody());

	// pop the function scope off the stack
	Interpret.symbolTable.popScope();

	// we are now in the parent scope of the function --
	// we have to restore the original scope on the top of the stack
	Interpret.symbolTable.setCurrentScope(topOfStack);

	// statements do not have types
	return Value.NOTYPE;
    }

    // variable declaration statements
    private int interp(VarDeclStmt ast) {

	int declType = ast.getType();
	String varName = ast.getVar();

	Value value;

	// build a value that we can enter in the symbol table as a proxy for the
	// actual computed value
	switch(declType) {
	case Value.INTEGER:
	    value = new IntConst(new Integer(0));
	    break;
	case Value.FLOAT:
	    value = new FloatConst(new Float(0.0));
	    break;
	case Value.STRING:
	    value = new StringConst("");
	    break;
	default:
	    System.err.println("Error: unknown type in VarDeclStmt.");
	    System.exit(1);
	    value = null;
	}

	// declare the variable with its init value
	Interpret.symbolTable.declareSymbol(ast.getVar(),value);

	// type check the init expression
	int initType = this.dispatch(ast.getAST(0));

	// check if this makes sense
	int resultType = Value.getResultType(declType,initType);

	// check for type errors
	if (resultType == Value.NOTYPE || resultType != declType) {
	    System.err.println("Error (VarDeclSstmt): expression type "+resultType+" cannot be assigned to variable of type "+declType);
	    System.exit(1);
	    return Value.NOTYPE;
	}

	// check if we have to insert a type promotion
	if (resultType != initType) {
	    AST newAst = new CastExpr(initType,resultType,(Expr)ast.getAST(0));
	    ast.putAST(0,newAst);
	}

	// statements do not have types
	return Value.NOTYPE;
    }

    // call statements -- function calls as statements -- ignore the return value
    private int interp(CallStmt ast) {

	// lookup the function value of the function name
	Function fValue = (Function)Interpret.symbolTable.lookupSymbol(ast.getFunctionName());
	
	// all we need to do here is to type check the parameters
	typecheckParameters(fValue.getFormalParameters(),ast.getActualParameters());

	// statements do not have types
	return Value.NOTYPE;
    }

    // return statements
    private int interp(ReturnStmt ast) {

	Function fValue = Interpret.symbolTable.getCurrentScope().getFunctionValue();

	if (fValue == null) {
	    System.err.println("Error: return statement not within a function.");
	    System.exit(1);
	    return Value.NOTYPE;
	}
	else if (ast.size() != 0) {
	    // get the function return type
	    int returnType = fValue.getReturnType();

	    // typecheck the return expression
	    int  exprType = this.dispatch((Expr)ast.getAST(0));

	    // make sure the return expression typechecks with the 
	    // function return type
	    int resultType = Value.getResultType(returnType,exprType);

	    // check for type errors
	    if (resultType == Value.NOTYPE || resultType != returnType) {
		System.err.println("Error (returnstmt): expression type "+resultType+" cannot be returned by function of type  "+returnType);
		System.exit(1);
		return Value.NOTYPE;
	    }

	    // check if we have to insert a type promotion
	    if (resultType != exprType) {
		AST newAst = new CastExpr(exprType,resultType,(Expr)ast.getAST(0));
		ast.putAST(0,newAst);
	    }
	}
	return Value.NOTYPE;
    }

    //****** typecheck expression level ASTs
    // binop expressions
    private int interp(BinopExpr ast) {

	// typecheck left child
	int leftType = this.dispatch(ast.getAST(0));

	// typecheck right child
	int rightType = this.dispatch(ast.getAST(1));

	// see if the expression is well typed
	int resultType = Value.getResultType(leftType,rightType);

	// check for type errors
	// NOTE: add on type string is string concatenation
	if (resultType == Value.NOTYPE) {
	    System.err.println("Error (binopexpr): binop expression with types "+leftType+" and  "+rightType+" is ill-typed");
		System.exit(1);
		return Value.NOTYPE;
	    }

	// check if we have to insert a type promotion
	if (resultType != leftType) {
	    AST newAst = new CastExpr(leftType,resultType,(Expr)ast.getAST(0));
	    ast.putAST(0,newAst);
	}
	    
	// check if we have to insert a type promotion
	if (resultType != rightType) {
	    AST newAst = new CastExpr(leftType,resultType,(Expr)ast.getAST(1));
	    ast.putAST(1,newAst);
	}
	    
	// the result type is correct except for the relational operators which
	// always construct an integer return value.
	if (ast.getOp() == BinopExpr.EQ || ast.getOp() == BinopExpr.LESSEQ) {
	    return Value.INTEGER;
	}
	else {
	    return resultType;
	}
    }

    // const expressions
    private int interp(ConstExpr ast) {
	return ast.getType();
    }

    // parenthesized expressions
    private  int interp(ParenExpr ast) {
	return this.dispatch(ast.getAST(0));
    }

    // variable expressions
    private int interp(VarExpr ast) {
	// fetch the variable value from symbol table
	Value value = Interpret.symbolTable.lookupSymbol(ast.getVarName());

	// make sure we are actually looking at a variable
	if (value.getType() == Value.FUNCTION) {
	    System.err.println("Error: function name "+ast.getVarName()+" not valid in the context of an expression.");
	    System.exit(1);
	    return Value.NOTYPE;
	}

	return value.getType();
    }

    // call expressions - call to functions within expressions -- have
    // deal with the return value.
    private int interp(CallExpr ast) {

	// lookup the function value of the function name
	Function fValue = (Function)Interpret.symbolTable.lookupSymbol(ast.getFunctionName());
	
	// all we need to do here is to type check the parameters
	typecheckParameters(fValue.getFormalParameters(),ast.getActualParameters());

	// return the return type of the function
	return fValue.getReturnType();
    }

}