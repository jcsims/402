// implementation of const expression

public class ConstExpr extends Expr {

    private Value value;

    public ConstExpr(Value v) {
	value = v;
    }

    public Value getValue() {
	return value;
    }

    public int getType() {
	return value.getType();
    }
}
