// an exception object that holds our function return value

public class ReturnValueException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Value returnValue = null;

    public ReturnValueException (Value returnValue) {
	this.returnValue = returnValue;
    }

    public Value getReturnValue () {
	return this.returnValue;
    }
}