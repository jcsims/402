// variable declaration statement

public class VarDeclStmt extends Stmt {

    private int type;
    private String var;

    public VarDeclStmt(int type,String v, Expr e) {
	this.type=type;
	this.var = v;
	this.addAST(e);
    }

    public int getType() {
	return type;
    }

    public String getVar() {
	return var;
    }

    public Expr getValue() {
	return (Expr) this.getAST(0);
    }
}