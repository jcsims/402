// implementation of the noop instruction

public class NoOpInstr extends Instr {

    public void executeInstr() {
	// implement behavior
	// this instruction doesn't do anything except transferring control
	// to the next instruction.
	executeNextInstr();
    }
}