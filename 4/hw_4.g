grammar hw_4;


//******************************************************************************
// this implements an interpreter for the exp1 bytecode, it uses an
// abstract notion of instruction as its intermediate representation

@header {
import java.util.HashMap;
import java.util.Set;
import java.util.Iterator;
import java.util.Map;
import java.util.Stack;
}
@members{
    public Program program = new Program();
    public Stack<Integer> stack = new Stack<Integer>();
    public HashMap<String, Integer> memory = new HashMap<String, Integer>();
    public HashMap<String, Instr> labelTable = new HashMap();
}

//******************************************************************************
// grammar rules

prog
scope {
    // lock variable def in prog
    String currentLabelDef;
}
    : (
            { $prog::currentLabelDef = null; }
            (l=labelDef { $prog::currentLabelDef = $l.name; } )?
            s=stmt
            {
                if ($prog::currentLabelDef!=null)
                    labelTable.put($prog::currentLabelDef,$s.instr);
                program.addInstr($s.instr);
            }
        )+
    ;

labelDef returns [String name]
    :   VAR ':'        { $name = $VAR.text; }
    ;

stmt returns [Instr instr]
    : 'push' (num=NUM {$instr = new PushInstr(new Integer($num.text),stack);}
        | var=lhsvar {$instr = new PushInstr($var.name, stack, memory);}) ';'
    |  'pop' ';'  { $instr = new PopInstr(stack);}
    | 'print' ';'       { $instr = new PrintInstr(stack); }
    | 'store' v=lhsvar ';'
        { $instr = new StoreInstr($v.name, memory, stack); }
    |  'ask' (msg=STRING)? ';' { $instr = new AskInstr($msg.text, stack);}
    |  'dup' ';'  {$instr = new DupInstr(stack);}
    |  'add' ';'  {$instr = new AddInstr(stack);}
    |  'sub' ';'  {$instr = new SubInstr(stack);}
    |  'mul' ';'  {$instr = new MulInstr(stack);}
    |  'div' ';'  {$instr = new DivInstr(stack);}
    |  'equ' ';'  {$instr = new EquInstr(stack);}
    |  'leq' ';'  {$instr = new LeqInstr(stack);}
    | 'jumpT' l=label ';'
        { $instr = new JumpTInstr($l.name,labelTable, stack); }
    | 'jumpF' l=label ';'
        { $instr = new JumpFInstr($l.name,labelTable, stack); }
    | 'jump' l=label ';'      { $instr = new JumpInstr($l.name,labelTable); }
    | 'stop' (msg=STRING)? ';'          { $instr = new StopInstr($msg.text); }
    | 'noop' ';'          { $instr = new NoOpInstr(); }
    ;


label returns [String name] :   VAR    { $name = $VAR.text; };
lhsvar returns [String name]    :   VAR    { $name = $VAR.text; };
rhsvar returns [String name]    :   VAR    { $name = $VAR.text; };

//******************************************************************************
// lexical rules
VAR : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')* ;
NUM : '-'?'0'..'9'+;
COMMENT : '//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;};
WS : ( ' ' | '\t' | '\r' | '\n' )       {$channel=HIDDEN;};
STRING : '"' ( ~('\\'|'"') | ESC_SEQ )* '"';
ESC_SEQ : '\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\');
