// implementation of the add instruction

import java.util.*;

public class AddInstr extends Instr {

    private Stack<Integer> stack = null;

    public AddInstr(Stack<Integer> stack) {
        this.stack = stack;
    }


    public void executeInstr() {
        // implement the behavior
        if (stack.size() < 2) {
            System.out.println("Error: stack has less than 2 items, cannot add!");
        } else {
            Integer temp = stack.pop();
            Integer temp2 = stack.pop();
            stack.push(temp + temp2);
            // transfer control to the next instuction
            executeNextInstr();
        }
    }
}
