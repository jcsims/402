// implementation of the mul instruction

import java.util.*;

public class MulInstr extends Instr {

    private Stack<Integer> stack = null;

    public MulInstr(Stack<Integer> stack) {
        this.stack = stack;
    }


    public void executeInstr() {
        // implement the behavior
        if (stack.size() < 2) {
            System.out.println("Error: stack has less than 2 items, cannot mul!");
        } else {
            Integer temp = stack.pop();
            Integer temp2 = stack.pop();
            stack.push(temp * temp2);
            // transfer control to the next instuction
            executeNextInstr();
        }
    }
}
