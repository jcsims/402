// implementation of the store instruction

import java.util.*;

public class StoreInstr extends Instr {

    private String var;
    private Stack<Integer> stack;
    private HashMap<String, Integer> memory;

    public StoreInstr(String var, HashMap<String, Integer> memory,
                      Stack<Integer> stack) {
        this.var = var;
        this.memory = memory;
        this.stack = stack;
    }

    public void executeInstr() {
        // implement the behavior
        if (stack.empty()) {
            System.out.println("Error: stack is empty, cannot store var!");
        } else {
            memory.put(var,stack.pop());

            // transfer control to the next instuction
            executeNextInstr();
        }
    }
}
