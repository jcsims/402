// implementation of the stop instruction

public class StopInstr extends Instr {

    private String msg;

    public StopInstr(String msg){
        this.msg = msg;
    }

    public void executeInstr() {
        // implement behavior
        if (msg != null) {
            System.out.println(msg);
        } else {
            System.out.println("Execution terminated with 'stop' statement.");
        }
    }
}
