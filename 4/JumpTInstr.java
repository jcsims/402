// implementation of the jumpt instruction

import java.util.*;

public class JumpTInstr extends Instr {

    private String label;
    private HashMap<String, Instr> labelTable;
    private Stack<Integer> stack;

    public JumpTInstr(String label, HashMap<String, Instr>
                      labelTable, Stack<Integer> stack) {
        this.label = label;
        this.labelTable = labelTable;
        this.stack = stack;
    }

    public void executeInstr() {
        // implement behavior
        if (stack.empty()) {
            System.out.println("Error: stack is empty, cannot jumpT!");
        } else {
            // if the top of the stack != 0 then transfer control to label
            // otherwise execute the next instruction in line
            if (stack.pop() != 0) {
                Instr i = (Instr)labelTable.get(label);
                // start executing this instruction
                i.executeInstr();
            }
            else {
                executeNextInstr();
            }
        }
    }
}
