// implementation of the jump instruction

import java.util.*;

public class JumpInstr extends Instr {
    
    private String labelName;
    private HashMap labelTable;

    public JumpInstr(String l, HashMap t) {
	labelName= l;
	labelTable = t;
    }

    public void executeInstr() {
	// implement behavior of unconditional jump
	Instr i = (Instr)labelTable.get(labelName);
	// start executing this instruction
	i.executeInstr();
    }
}
