// an exception object that signifies a break in a loop

public class BreakException extends Exception {

    private static final long serialVersionUID = -8814284479189541709L;

}
