// argument pair (name -> value)

public class ArgPair extends AST {

    public String varName;

    public ArgPair() {
    }

    public ArgPair(String varName, VarExpr e) {
        this.varName = varName;
	this.addAST(e);
    }

    public ArgPair(String varName, Expr e) {
        this.varName = varName;
	this.addAST(e);
    }
}
