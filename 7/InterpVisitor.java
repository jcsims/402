// this visitor class implements the simple3 interpreter.

import java.util.*;
import java.io.*;

public class InterpVisitor {

    // reading the console input
    private InputStreamReader converter = new InputStreamReader(System.in);
    private BufferedReader consoleInput = new BufferedReader(converter);

    // initializing function parameters in the current scope
    // this function implements keyword parameter correspondence.
    private void initParameters(ArgList formalParameters, ArgList actualParameters)
        throws ReturnValueException, BreakException {

        // first check that the list sizes are the same
        if (formalParameters.size() != actualParameters.size()) {
            System.err.println("Error (initParameters): Not enough parameters passed.");
            System.exit(1);
        }

        // Build the map of actual parameters passed
        HashMap<String, ArgPair> params = new HashMap<String, ArgPair>();
        for (int i = 0; i < actualParameters.size(); i++) {
            ArgPair param = (ArgPair) actualParameters.getAST(i);
            params.put(param.varName, param);
        }

        // make sure the named parameters match
        for (int i = 0; i < formalParameters.size(); i++) {
            String varName = ((VarExpr)formalParameters.getAST(i)).getVarName();
            if (!params.containsKey(varName)) {
                System.err.print("Error (initParameters): ");
                System.err.println("No param passed to match: " + varName);
                System.exit(1);
            }
        }

        // now simply step through the map and declare the formal
        // parameters as local variables
        for (String param : params.keySet()) {
            Integer value = this.dispatch(params.get(param));
            Interpret.symbolTable.declareVariable(param, value);
        }
    }

    // the dispatcher for the interpreter visitor
    public Integer dispatch(AST ast) throws ReturnValueException, BreakException {
        if      (ast.getClass() == AssignStmt.class) return interp((AssignStmt)ast);
        else if (ast.getClass() == BlockStmt.class) return interp((BlockStmt)ast);
        else if (ast.getClass() == GetStmt.class) return interp((GetStmt)ast);
        else if (ast.getClass() == IfStmt.class) return interp((IfStmt)ast);
        else if (ast.getClass() == PutStmt.class) return interp((PutStmt)ast);
        else if (ast.getClass() == WhileStmt.class) return interp((WhileStmt)ast);
        else if (ast.getClass() == StmtList.class) return interp((StmtList)ast);
        else if (ast.getClass() == BinopExpr.class) return interp((BinopExpr)ast);
        else if (ast.getClass() == NumExpr.class) return interp((NumExpr)ast);
        else if (ast.getClass() == ParenExpr.class) return interp((ParenExpr)ast);
        else if (ast.getClass() == VarExpr.class) return interp((VarExpr)ast);
        else if (ast.getClass() == FuncDeclStmt.class) return interp((FuncDeclStmt)ast);
        else if (ast.getClass() == VarDeclStmt.class) return interp((VarDeclStmt)ast);
        else if (ast.getClass() == CallStmt.class) return interp((CallStmt)ast);
        else if (ast.getClass() == CallExpr.class) return interp((CallExpr)ast);
        else if (ast.getClass() == ReturnStmt.class) return interp((ReturnStmt)ast);
        else if (ast.getClass() == ArgPair.class) return interp((ArgPair)ast);
        else if (ast.getClass() == BreakStmt.class) return interp((BreakStmt)ast);
        else {
            System.out.println("Error (InterpVisitor): unknown class type: " +
                               ast.getClass().toString());
            System.exit(1);
            return null;
        }
    }

    //****** interpret statement level ASTs
    // assignment statements
    private Integer interp(AssignStmt ast) throws ReturnValueException, BreakException {

        // evaluate the expression
        Integer value = this.dispatch(ast.getAST(0));
        // assign the value to the lhs var
        Interpret.symbolTable.updateVariable(ast.lhsVar(),value);

        // statements do not have return values -- null
        return null;
    }

    // block statements
    private Integer interp(BlockStmt ast) throws ReturnValueException, BreakException {
        // set up the scope for this block
        Interpret.symbolTable.pushScope();

        // interpret each of the statements in the block
        for (int i = 0; i < ast.size(); i++) {
            this.dispatch(ast.getAST(i));
        }

        // leaving this scope -- set current scope to parent scope
        Interpret.symbolTable.popScope();

        // statements do not have return values -- null
        return null;
    }

    // get statements
    private Integer interp(GetStmt ast) {

        try {
            // get the value string from the user
            System.out.print("Enter a value: ");
            String inputString = consoleInput.readLine();
            Integer value = new Integer(inputString);

            // assign the value to the lhs var
            Interpret.symbolTable.updateVariable(ast.lhsVar(),value);

            // statements do not have return values -- null
            return null;
        }
        catch(IOException e) {
            System.out.println("Interpreter Visitor: exception " + e);
            System.exit(1);
            return null;
        }
    }

    // if statements
    private Integer interp(IfStmt ast) throws ReturnValueException, BreakException {
        // interpret the expression
        Integer value = this.dispatch(ast.getAST(0));

        if (value.intValue() != 0) {
            // interpret the then clause
            this.dispatch(ast.getAST(1));
        }
        else if (ast.size() == 3) {
            // interpret the else clause if we have one
            this.dispatch(ast.getAST(2));
        }

        // statements do not have return values -- null
        return null;
    }

    // put statements
    private Integer interp(PutStmt ast) throws ReturnValueException, BreakException {
        // interpret the expression
        Integer value = this.dispatch(ast.getAST(0));
        System.out.println("Output Value: " + value.toString());

        // statements do not have return values -- null
        return null;
    }

    // while statements
    private Integer interp(WhileStmt ast) throws ReturnValueException {
        Integer value = null;

        // interpet the expression
        try{
            value = this.dispatch(ast.getAST(0));
        } catch (BreakException e) {
            // Another case of the grammar not allowing
            // program execution to get to this point
            // Do nothing
        }

        // interpret the loop while the expression value != 0
        while (value.intValue() != 0) {
            // interpret the while body
            try{
                this.dispatch(ast.getAST(1));
                // reevaluate the loop expression
                value = this.dispatch(ast.getAST(0));
            } catch (BreakException e) {
                break;
            }
        }

        // statements do not have return values -- null
        return null;
    }

    // statement lists
    private Integer interp(StmtList ast) throws ReturnValueException {

        // interpret each of the statements
        for (int i = 0; i < ast.size(); i++) {
            try{
                this.dispatch(ast.getAST(i));
            } catch (BreakException e) {
                // This is the top level of the program,
                // so this halts the program
                System.out.println("Halting execution due to break statement.");
                System.exit(0);
            }
        }

        // statements do not have return values -- null
        return null;
    }


    // function declaration statements
    private Integer interp(FuncDeclStmt ast) {

        Function fValue = ast.Value();

        // declare the function
        Interpret.symbolTable.declareFunction(ast.Name(),fValue);

        // statements do not have return values -- null
        return null;
    }

    // variable declaration statements
    private Integer interp(VarDeclStmt ast) throws ReturnValueException, BreakException {

        // evaluate the init expression
        Integer value = this.dispatch(ast.getAST(0));
        // declare the variable with its init value
        Interpret.symbolTable.declareVariable(ast.Var(),value);

        // statements do not have return values -- null
        return null;
    }

    // call statements -- function calls as statements -- ignore the return value
    private Integer interp(CallStmt ast) throws ReturnValueException, BreakException {

        // lookup the function value of the function name
        Function fValue = Interpret.symbolTable.lookupFunction(ast.getFunctionName());

        // push function local scope onto the stack
        Interpret.symbolTable.pushScope();
        // Initialize formal parameters with actual parameters in the current scope
        initParameters(fValue.getFormalParameters(),ast.getActualParameters());

        // execute the body of the function
        try {
            this.dispatch(fValue.getFunctionBody());
        }
        catch (ReturnValueException e) {
            // ignore!
        }

        // pop the function scope off the stack
        Interpret.symbolTable.popScope();

        // statements do not have return values -- null
        return null;
    }

    // return statements
    private Integer interp(ReturnStmt ast) throws ReturnValueException, BreakException {

        if (ast.size() == 0) {
            // no return value express, throw a null
            // return value object.
            throw new ReturnValueException(null);
        }
        else {
            // evaluate the return expression
            Integer returnValue = this.dispatch(ast.getExpr());

            // now throw the return value object
            throw new ReturnValueException(returnValue);
        }
    }

    // break statements
    private Integer interp(BreakStmt ast) throws BreakException {
        throw new BreakException();
    }

    //****** interpret expression level ASTs
    // binop expressions
    private Integer interp(BinopExpr ast) throws ReturnValueException, BreakException {

        // interpret left child
        Integer left = this.dispatch(ast.getAST(0));
        // interpret right child
        Integer right = this.dispatch(ast.getAST(1));

        // compute the return value based on the OP
        switch(ast.getOp()) {
        case BinopExpr.ADD:
            return new Integer(left.intValue() + right.intValue());
        case BinopExpr.MINUS:
            return new Integer(left.intValue() - right.intValue());
        case BinopExpr.MULT:
            return new Integer(left.intValue() * right.intValue());
        case BinopExpr.DIV:
            return new Integer(left.intValue() / right.intValue());
        case BinopExpr.EQ:
            return new Integer((left.intValue() == right.intValue())?1:0);
        case BinopExpr.LESSEQ:
            return new Integer((left.intValue() <= right.intValue())?1:0);
        default:
            System.out.println("Error (InterpVisitor): unknown binary operator expression.");
            System.exit(1);
            return null;
        }
    }

    // number expressions
    private Integer interp(NumExpr ast) {
        return new Integer(ast.getValue());
    }

    // parenthesized expressions
    private  Integer interp(ParenExpr ast) throws ReturnValueException, BreakException {
        return this.dispatch(ast.getAST(0));
    }

    // rhs variable expressions
    private Integer interp(VarExpr ast) {
        // fetch the variable value from symbol table
        return Interpret.symbolTable.lookupVariable(ast.getVarName());
    }

    // argpair expressions
    private Integer interp(ArgPair ast) throws ReturnValueException, BreakException {
        // evaluate the passed expression
        return this.dispatch(ast.getAST(0));
    }

    // call expressions - call to functions within expressions -- have
    // deal with the return value.
    private Integer interp(CallExpr ast) throws ReturnValueException, BreakException {

        // lookup the function value of the function name
        Function fValue = Interpret.symbolTable.lookupFunction(ast.getFunctionName());

        // implement dynamic scoping
        // push function local scope onto the stack
        Interpret.symbolTable.pushScope();
        // Initialize formal parameters with actual parameters in the current scope
        initParameters(fValue.getFormalParameters(),ast.getActualParameters());

        // execute the body of the function and retrieve the return value
        Integer returnValue = null;
        try {
            this.dispatch(fValue.getFunctionBody());
            // if we got here then we did not receive a
            // return value exception -- error!
            System.err.println("Error: expected return value from function '"+
                               ast.getFunctionName()+"'.");
            System.exit(1);
        }
        catch (ReturnValueException e) {
            returnValue = e.getReturnValue();

            // if this is null then we saw a return without a value
            if (returnValue == null) {
                System.err.println("Error: expected return value from function '"
                                   +ast.getFunctionName()+"'.");
                System.exit(1);
            }
        }

        // pop the function scope off the stack
        Interpret.symbolTable.popScope();

        // the return value of a function call is the value of the
        // called function
        return returnValue;
    }

}
