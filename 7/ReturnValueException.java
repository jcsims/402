// an exception object that holds our function return value

public class ReturnValueException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = -8814284479189541709L;
	private Integer returnValue = null;

    public ReturnValueException (Integer returnValue) {
	this.returnValue = returnValue;
    }

    public Integer getReturnValue () {
	return this.returnValue;
    }
}