// function value

public class Function extends AST {

    // We're implementing dynamic scoping, so we'll
    // let the implementation handle traveling up the
    // stack to find variables
    public Function(ArgList formalParameters, Stmt body) {
	this.addAST(formalParameters);
	this.addAST(body);
    }

    public Function(Stmt body) {
	// formal parameter list is empty
	this.addAST(new ArgList());
	this.addAST(body);
    }

    public ArgList getFormalParameters() {
	return (ArgList) this.getAST(0);
    }

    public Stmt getFunctionBody() {
	return (Stmt) this.getAST(1);
    }

}
