package jcsi.ms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * CSC 402 Assignment 1
 * A simple parser for the exp0 language:
 * prog : stmt+ ;
 * stmt : 'p' exp ';' | 's’ lhsvar exp ';' ;
 * exp : '+' exp exp | '-' exp exp | '(' exp ')' | rhsvar | num ;
 * lhsvar : 'x' | 'y' | 'z' ;
 * rhsvar : 'x' | 'y' | 'z' ;
 * num : '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' |'9' ; 
 *
 */
@SuppressWarnings("unchecked")
public class Parser 
{
    // Keywords used in the state map
    private static final String NUM_P = "num_p";
    private static final String ERROR = "error";
    private static final String STATEMENT = "stmt";

    public static void main( String[] args )
    {
        if (args.length > 0) {
            System.out.println("Parsing statement: " + args[0]);
        }
        else {
            System.out.println("Error: no statment to parse!");
            System.exit(1);
        }
        
        HashMap<String, Object> results = Parser.parse(args[0]);
        if (!results.containsKey(ERROR)) {
            System.out.printf("Successfully parsed statement: %s \n", args[0]); 
            System.out.printf("Contained %d p's", (Integer) results.get(NUM_P));
        } 
        else {
            System.out.println((String) results.get(ERROR));
        }
        
    }
    
    // Main parsing function. Returns the resulting parse map
    public static HashMap<String, Object> parse(String statement) {
        HashMap<String, Object> results = new HashMap<String, Object>();
        results.put(NUM_P, 0);
        
        results.put(STATEMENT, splitInput(statement));
        
        // Start parsing!
        results = prog(results);
        
	return results;
    }
    
    
	private static HashMap<String, Object> prog(HashMap<String, Object> results) {
        while (((ArrayList<String>) results.get(STATEMENT)).size() > 0) {
            stmt(results);
        }
        return results;
    }

    private static HashMap<String, Object> stmt(HashMap<String, Object> results) {
        String next = peekToken((ArrayList<String>) results.get(STATEMENT));
        if (next != null ) {
            switch(next) {
            case "p":
                incP(results);
                exp(results);
                next = peekToken((ArrayList<String>) results.get(STATEMENT));
                if (next == null || !next.equalsIgnoreCase(";")) {
                    results.put(ERROR, "Parsing error: missing ';'");
                } 
                break;
            case "s":
                lhsvar(results);
                exp(results);
                next = peekToken((ArrayList<String>) results.get(STATEMENT));
                if (next == null || !next.equalsIgnoreCase(";")) {
                    results.put(ERROR, "Parsing error: missing ';'");
                } 
                break;
            default:
                results.put(ERROR, "Parsing error: missing 's' or 'p'");
                break;
            }
        } else {
        	results.put(ERROR, "Parsing error: expected stmt, got nothing");
        }
        return results;
    }
    
    private static HashMap<String, Object> exp(HashMap<String, Object> results) {
        String next = peekToken((ArrayList<String>) results.get(STATEMENT));
        if (next != null ) {
            switch(next) {
            case "+":
                exp(results);
                exp(results);
                break;
            case "-":
                exp(results);
                exp(results);
                break;
            case "(":
                exp(results);
                next = peekToken((ArrayList<String>) results.get(STATEMENT));
                if (next == null || !next.equalsIgnoreCase(")")) {
                    results.put(ERROR, "Parsing error: missing ')'");
                } 
                break;
            case "x":
            case "y":
            case "z":
            case "0":
            case "1":
            case "2":
            case "3":
            case "4":
            case "5":
            case "6":
            case "7":
            case "8":
            case "9":
                // Nothing to do here...
                break;
            default:
                results.put(ERROR, "Parsing error: expecting 'exp', not found");
                break;
            }
        } else {
        	results.put(ERROR, "Parsing error: Expected exp, got nothing");
        }
        return results;
    }

    private static HashMap<String, Object> lhsvar( HashMap<String, Object> results) {
        String next = peekToken((ArrayList<String>) results.get(STATEMENT));
        if (next != null ) {
            switch(next) {
            case "x":
            case "y":
            case "z":
                // Great success!
                break;
            default:
                results.put(ERROR, "Parsing error on lhsvar - missing 'x|y|z'");
                break;
            }
        }
        else {
        	results.put(ERROR, "Parsing error: expected lhsvar, got nothing");
        }
        return results;
    }

    // Split the input into an array, with one character per entry
    private static ArrayList<String> splitInput(String input) {
        // Remove whitespace, split on any single char besides
        // beginning of line
        String[] cleaned = removeWhitespace(input).split("(?!^)");
        return new ArrayList<String>(Arrays.asList(cleaned));
    }
    
    // Remove all whitespace characters in the input string
    private static String removeWhitespace(String input) {
        return input.replaceAll("\\s","");
    }
    
    // Peek at the first character available
    private static String peekToken(ArrayList<String> input) {
        String result = null;
        if (input.size() > 0) {
            result = input.remove(0); 
        } 
        return result;
    }
    
    // Increment the number of p's that we've found
    private static void incP(HashMap<String, Object> results) {
        int num_p = (Integer) results.get(NUM_P);
        results.put(NUM_P, ++num_p);
    }

}
