grammar exp1x;

// Modifications to this grammar borrow heavily from the textbook
// "The Definitive ANTLR Reference", Section 3.2

options{k=1;}

@header {
import java.util.HashMap;
}

@members {
    HashMap<String,Integer> memory = new HashMap<String, Integer>();
}

prog:   stat+ ;

stat:   // Print out the resulting expression
        'print' expr ';' NEWLINE {System.out.println($expr.value);}

        // Store the result of the expression in the memory hashmap
    |   'store' ID expr ';' NEWLINE
        {memory.put($ID.text, new Integer($expr.value));}

        // Nothing to do here, move along
    |   NEWLINE
    ;

// Treat the resultant list from this regular expression as an
// acumulator, based on the operator
expr returns [int value]
    :   e=multExpr {$value = $e.value;}
        ('+' e=multExpr {$value += $e.value;}
        |'-' e=multExpr {$value -= $e.value;}
        )*
    ;

multExpr returns [int value]
    :   e=atom {$value = $e.value;}
        ('*' e=atom {$value *= $e.value;}
        |'/' e=atom
            // We need to ensure that we're not dividing by zero
            {
                if ($e.value == 0) System.err.println("Error: division by zero.");
                else $value /= $e.value;
            }
        )*
    ;

atom returns [int value]
    :   // The value of an int is just itself
        INT {$value = Integer.parseInt($INT.text);}

        // This is a variable, which should be in the hashmap
    |   ID
        {
            Integer v = memory.get($ID.text);
            // The return value is null if there is no mapping for this
            // var
            if (v != null) $value = v.intValue();
            else System.err.println("undefined variable " + $ID.text);
        }

        // Expressions in parenthesis just evaluate to the expression itself
    |   '(' expr ')' {$value = $expr.value;}
    ;

ID  :   ('a'..'z'|'A'..'Z')+ ;
INT :   '0'..'9'+ ;
NEWLINE:'\r'? '\n' ;
COMMENT :   '//' ~('\n'|'\r')* '\r'? '\n'   { $channel=HIDDEN; };
WS      :   ( ' ' | '\t' | '\r' | '\n' )    { $channel=HIDDEN; };
