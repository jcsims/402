import org.antlr.runtime.*;

public class Exp1xDriver {
    public static void main(String[] args)  throws Exception {
        if (args.length == 0) {
            System.out.println("Usage: java Exp1xDriver <input file>");
            System.exit(0);
        } else {
            System.out.println("Processing: " + args[0]);
        }
        // set up and initialize our lexer and parser objects
        // open up the input file
        ANTLRFileStream input = new ANTLRFileStream(args[0]);
        // create the lexer with the input stream
        exp1xLexer lexer = new exp1xLexer(input);
        // create a token stream for the parser
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        // create a parser object with the token stream
        exp1xParser parser = new exp1xParser(tokens);
        // call the toplevel recursive descent parsing function -- start symbol
        parser.prog();
    }
}
