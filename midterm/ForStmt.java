// for statement

public class ForStmt extends Stmt {
    
    public String id;
    public String id2;
    public Expr start;
    public Expr end;
    public Expr step;
    public BlockStmt block;

    public ForStmt(String id, Expr start, Expr end, Expr step) {
        this.id = id;
        this.start = start;
        this.end = end;
        this.step = step;
        this.id2 = null;
        this.block = new BlockStmt();
    }
    
    public void addStmt(Stmt ast) {
        block.addAST(ast);
    }
}
