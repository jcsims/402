// this class implements implements binary operators

public class BinopExpr extends Expr {

    public enum oper {
        ADD, MINUS, MULT, DIV, EQ, LESSEQ, AND, OR
    }

        oper op;

    public BinopExpr(oper o, Expr e1, Expr e2) {
        op = o;
        this.addAST(e1);
        this.addAST(e2);
    }

    public oper getOp() {
        return op;
    }
}
