import java.util.*;
import java.io.*;

public class InterpVisitor {

    // our variable value memory.
    private HashMap<String, Integer> memory = new HashMap();

    // reading the console input
    private InputStreamReader converter = new InputStreamReader(System.in);
    private BufferedReader consoleInput = new BufferedReader(converter);

    // the dispatcher for the interpreter visitor
    public Integer dispatch(AST ast) {
        if      (ast.getClass() == AssignStmt.class) return interp((AssignStmt)ast);
        else if (ast.getClass() == BinopExpr.class) return interp((BinopExpr)ast);
        else if (ast.getClass() == BlockStmt.class) return interp((BlockStmt)ast);
        else if (ast.getClass() == EndStmt.class) return interp((EndStmt)ast);
        else if (ast.getClass() == ForStmt.class) return interp((ForStmt)ast);
        else if (ast.getClass() == IdExpr.class) return interp((IdExpr)ast);
        else if (ast.getClass() == IfStmt.class) return interp((IfStmt)ast);
        else if (ast.getClass() == InputStmt.class) return interp((InputStmt)ast);
        else if (ast.getClass() == IntExpr.class) return interp((IntExpr)ast);
        else if (ast.getClass() == NegateExpr.class) return interp((NegateExpr)ast);
        else if (ast.getClass() == NumExpr.class) return interp((NumExpr)ast);
        else if (ast.getClass() == ParenExpr.class) return interp((ParenExpr)ast);
        else if (ast.getClass() == PrintStmt.class) return interp((PrintStmt)ast);
        else if (ast.getClass() == StmtList.class) return interp((StmtList)ast);
        else if (ast.getClass() == WhileStmt.class) return interp((WhileStmt)ast);
        else {
            System.out.println("PrettyPrintVisitor: unknown class type: "
                               + ast.getClass());
            System.exit(1);
            return null;
        }
    }

    //****** interpret statement level ASTs
    // statements
    private Integer interp(AssignStmt ast) {

        // evaluate the expression
        Integer value = this.dispatch(ast.getAST(0));
        // assign the value to the lhs var
        memory.put(ast.lhsVar(),value);

        // statements do not have return values -- null
        return null;
    }

    // block statements
    private Integer interp(BlockStmt ast) {

        // interpret each of the statements in the block
        for (int i = 0; i < ast.size(); i++) {
            this.dispatch(ast.getAST(i));
        }

        // statements do not have return values -- null
        return null;
    }

    // end the program
    private Integer interp(EndStmt ast) {
        System.exit(0);
        return 0;
    }

    // for statements
    private Integer interp(ForStmt ast) {
        // interpret the expression
        int start = this.dispatch(ast.start);
        int end = this.dispatch(ast.end);
        int step = 1;
        if (ast.step != null) {
            step = this.dispatch(ast.step);
        }
        if (step < 0) {
            for (int i = start; i >= end  ; i = i + step) {
                memory.put(ast.id, new Integer(i));
                //execute the block of statements in the for loop
                this.dispatch(ast.block);
                i = memory.get(ast.id);
            }
        }
        else {
            for (int i = start; i <= end  ; i = i + step) {
                memory.put(ast.id, new Integer(i));
                //execute the block of statements in the for loop
                this.dispatch(ast.block);
                i = memory.get(ast.id);
            }
        }

        // statements do not have return values -- null
        return null;
    }

    // if statements
    private Integer interp(IfStmt ast) {
        // interpret the expression
        Integer value = this.dispatch(ast.getAST(0));

        if (value.intValue() != 0) {
            // interpret the then clause
            this.dispatch(ast.getAST(1));
        }
        else if (ast.size() == 3) {
            // interpret the else clause if we have one
            this.dispatch(ast.getAST(2));
        }

        // statements do not have return values -- null
        return null;
    }

    // input statements
    private Integer interp(InputStmt ast) {

        try {
            // get the value string from the user
            if (ast.msg != null) {
                System.out.println(ast.msg);
            }
            else {
                System.out.print("Enter a value: ");
            }
            String inputString = consoleInput.readLine();
            Integer value = new Integer(inputString);

            // assign the value to the lhs var
            memory.put(ast.id,value);

            // statements do not have return values -- null
            return null;
        }
        catch(IOException e) {
            System.out.println("Interpreter Visitor: exception " + e);
            System.exit(1);
            return null;
        }
    }

    // print statements
    private Integer interp(PrintStmt ast) {
        // interpret the expression
        if (ast.string.startsWith("\"")) {
            // Just print the string
            System.out.print(ast.string.replace("\"", ""));
        }
        else {
            System.out.print(memory.get(ast.string));
        }

        // statements do not have return values -- null
        return null;
    }

    // while statements
    private Integer interp(WhileStmt ast) {
        Integer value;

        // interpet the expression
        value = this.dispatch(ast.getAST(0));

        // interpret the loop while the expression value != 0
        while (value.intValue() != 0) {
            // interpret the while body
            this.dispatch(ast.getAST(1));
            // reevaluate the loop expression
            value = this.dispatch(ast.getAST(0));
        }

        // statements do not have return values -- null
        return null;
    }

    // statement lists
    private Integer interp(StmtList ast) {

        // interpret each of the statements
        for (int i = 0; i < ast.size(); i++) {
            this.dispatch(ast.getAST(i));
        }

        // statements do not have return values -- null
        return null;
    }


    //****** interpret expression level ASTs
    // binop expressions
    private Integer interp(BinopExpr ast) {

        // interpret left child
        Integer left = this.dispatch(ast.getAST(0));
        // interpret right child
        Integer right = this.dispatch(ast.getAST(1));

        // compute the return value based on the OP
        switch(ast.getOp()) {
        case ADD:
            return new Integer(left.intValue() + right.intValue());
        case MINUS:
            return new Integer(left.intValue() - right.intValue());
        case MULT:
            return new Integer(left.intValue() * right.intValue());
        case DIV:
            return new Integer(left.intValue() / right.intValue());
        case EQ:
            return new Integer((left.intValue() == right.intValue())?1:0);
        case LESSEQ:
            return new Integer((left.intValue() <= right.intValue())?1:0);
        case AND:
            return new Integer(((left.intValue() != 0) && (right.intValue() != 0))
                               ? 1 : 0);
        case OR:
            return new Integer(((left.intValue() == 1) || (right.intValue() == 1))
                               ? 1 : 0);
        default:
            System.out.print("Interpreter Visitor: unknown binary");
            System.out.println("operator expression.");

            System.exit(1);
            return null;
        }
    }

    // id expressions
    private Integer interp(IdExpr ast) {
        return memory.get(ast.getId());
    }

    // int expressions
    private Integer interp(IntExpr ast) {
        return ast.value;
    }

    // negate expressions
    private Integer interp(NegateExpr ast) {
        Integer result = this.dispatch(ast.getAST(0));
        if (result == null) {
            System.out.println("Improper use of negation");
            System.exit(1);
        }
        else {
            return new Integer(result == 0 ? 1 : 0);
        }
        return null;
    }

    // number expressions
    private Integer interp(NumExpr ast) {
        return new Integer(ast.value);
    }

    // parenthesized expressions
    private  Integer interp(ParenExpr ast) {
        return this.dispatch(ast.getAST(0));
    }
}
