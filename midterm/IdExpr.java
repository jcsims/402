// implementation of ID expression

public class IdExpr extends Expr {
    private String id;

    public IdExpr(String id) {
	this.id = id;
    }    

    public String getId() {
	return id;
    }
}
