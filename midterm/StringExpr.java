// string expression

public class StringExpr extends Expr {

    private String string;
    public StringExpr(String e) {
	this.string = e;
    }
    
    public String getString() {
        return string;
    }
}
