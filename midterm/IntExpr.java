// implementation of int expression

public class IntExpr extends Expr {

    public int value;

    public IntExpr(int e) {
        value = e;
    }

    public IntExpr(String v) {
        value = Integer.parseInt(v);
    }
}
