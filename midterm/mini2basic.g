grammar mini2basic;

// Implements an interpreter for the mini2basic language
// This implementation is similar to the interpreter for the
// exp1 bytecode interpreter


prog returns [AST ast]
    :   { $ast = new StmtList(); } (stmt {$ast.addAST($stmt.ast);})+
    ;

stmt returns [Stmt ast]
    :   id=ID '=' e=exp {$ast = new AssignStmt($id.text, $e.ast);}
    |   'input' (msg=STRING ',')? id=ID
        {$ast = new InputStmt($msg.text, $id.text);}
    | {$ast = new BlockStmt();} 'print' v=value
        {$ast.addAST(new PrintStmt($v.string));}
        (',' v1=value {$ast.addAST(new PrintStmt($v1.string));})*
    |   'end' {$ast = new EndStmt();}
    | 'if' e=exp 'then' s1=stmt {BlockStmt ifBlock = new BlockStmt();
                                 ifBlock.addAST($s1.ast);
                                 $ast = new IfStmt(e, ifBlock);}
        (s2=stmt {ifBlock.addAST($s2.ast);})*
        ('else' s3=stmt { BlockStmt elseBlock = new BlockStmt();
                          elseBlock.addAST($s3.ast);
                          $ast.addAST(elseBlock);}
            (s4=stmt {elseBlock.addAST($s4.ast);} )*)? 'endif'
    |   'while' exp s1=stmt {BlockStmt block = new BlockStmt();
                             block.addAST($s1.ast);
                             $ast = new WhileStmt($exp.ast, block);}
        (s2=stmt {block.addAST($s2.ast);})* 'endwhile'
    |   'for' id=ID '=' e1=exp 'to' e2=exp ('step' e3=exp)?
        {$ast = new ForStmt($id.text,e1,e2,e3);}
        (s=stmt {((ForStmt) $ast).addStmt($s.ast);})+
        'next' id2=ID {((ForStmt)$ast).id2 = $id2.text;}
    ;

exp returns [Expr ast]
    :   e=logexp {$ast =$e.ast;};
logexp returns [Expr ast]
    :   e1=relexp {$ast = $e1.ast;}
        (
            '&' e2=relexp {$ast = new BinopExpr(BinopExpr.oper.AND,
                    $ast, $e2.ast);} |
            '|' e3=relexp {$ast = new BinopExpr(BinopExpr.oper.OR,
                    $ast, $e3.ast);}
        )*
    ;

relexp returns [Expr ast]
    :   e1=addexp {$ast = $e1.ast;}
        (
            '==' e2=addexp {$ast = new BinopExpr(BinopExpr.oper.EQ,
                    $ast, $e2.ast);} |
            '<=' e3=addexp {$ast = new BinopExpr(BinopExpr.oper.LESSEQ,
                    $ast, $e3.ast);}
        )*
    ;

addexp returns [Expr ast]
    :   e1=mulexp {$ast = $e1.ast;}
        (
            '+' e2=mulexp {$ast = new BinopExpr(BinopExpr.oper.ADD,
                    $ast, $e2.ast);} |
            '-' e3=mulexp {$ast = new BinopExpr(BinopExpr.oper.MINUS,
                    $ast, $e3.ast);}
        )*
    ;

mulexp returns [Expr ast]
    :   e1=atom { $ast = $e1.ast; }
        (
            ('*' e2=atom {$ast = new BinopExpr(BinopExpr.oper.MULT,
                        $ast,$e2.ast); }) |
            ('/' e3=atom { $ast = new BinopExpr(BinopExpr.oper.DIV,
                        $ast,$e3.ast); })
        )*
    ;

atom returns [Expr ast]
    :   '(' exp ')'     { $ast = new ParenExpr($exp.ast); }
    |   '!' a=atom   { $ast = new NegateExpr($a.ast); }
    |   ID { $ast = new IdExpr($ID.text);}
    |   num { $ast = new NumExpr($num.value);}
    ;

num returns [Integer value]
    :   INT {$value = Integer.parseInt($INT.text);}
    |   '-' INT {$value = Integer.parseInt('-' + $INT.text);}
    ;

value returns [String string]
    :   ID {$string = $ID.text;}
    |   num {$string = $num.value.toString();}
    |   STRING {$string = $STRING.text;}
    ;

ID  :   ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
    ;

INT :   '0'..'9'+
    ;

STRING
    :  '"' ( ESC_SEQ | ~('\\'|'"') )* '"'
    ;

fragment
ESC_SEQ
    :   '\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\')
    ;

COMMENT
    :   'rem' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    ;

WS  :   ( ' '
        | '\t'
        | '\r'
        | '\n'
        ) {$channel=HIDDEN;}
    ;
