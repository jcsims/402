/*******************************************************************************
 *  This is the toplevel driver program for the mini2basic  interpreter
 *  For language details please see the grammar file 'mini2basic.g'.
 *******************************************************************************/

import org.antlr.runtime.*;
import java.util.*;

public class Interpreter {
    public static void main(String[] args) throws Exception {

	// check if we have an input file, if not print out error and abort
	if (args.length == 0) {
	    System.out.println("Usage: java Interpreter <input file>");
	    System.exit(0);
	} else {
	    System.out.println("Processing: " + args[0]);
	}

	// set up and initialize our lexer and parser objects
	// open up the input file
        ANTLRFileStream input = new ANTLRFileStream(args[0]);
	// create the lexer with the input stream
        mini2basicLexer lexer = new mini2basicLexer(input);
	// create a token stream for the parser
        CommonTokenStream tokens = new CommonTokenStream(lexer);
	// create a parser object with the token stream
        mini2basicParser parser = new mini2basicParser(tokens);
	
	// call the toplevel recursive descent parsing function to construct
	// our IR
        AST ast = parser.prog();

	// interpret the IR
        InterpVisitor visitor = new InterpVisitor();
        visitor.dispatch(ast);

    }
}
