// print statement

public class PrintStmt extends Stmt {

    public String string;

    public PrintStmt(Expr ast) {
        this.addAST(ast);
    }

    public PrintStmt(String string) {
        this.string = string;
    }
}
