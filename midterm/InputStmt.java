// input statement

public class InputStmt extends Stmt {

    public String id;
    public String msg;

    public InputStmt(String msg, String id) {
        this.msg = msg.replace("\"", "");
        this.id = id;
    }

}
