// $ANTLR 3.2 debian-7 exp0.g 2013-09-26 10:08:01

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class exp0Lexer extends Lexer {
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int EOF=-1;
    public static final int T__9=9;
    public static final int T__8=8;
    public static final int T__7=7;
    public static final int T__6=6;
    public static final int T__5=5;
    public static final int T__19=19;
    public static final int WS=4;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int T__10=10;

    // delegates
    // delegators

    public exp0Lexer() {;} 
    public exp0Lexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public exp0Lexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "exp0.g"; }

    // $ANTLR start "T__5"
    public final void mT__5() throws RecognitionException {
        try {
            int _type = T__5;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // exp0.g:3:6: ( 'p' )
            // exp0.g:3:8: 'p'
            {
            match('p'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__5"

    // $ANTLR start "T__6"
    public final void mT__6() throws RecognitionException {
        try {
            int _type = T__6;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // exp0.g:4:6: ( ';' )
            // exp0.g:4:8: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__6"

    // $ANTLR start "T__7"
    public final void mT__7() throws RecognitionException {
        try {
            int _type = T__7;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // exp0.g:5:6: ( 's' )
            // exp0.g:5:8: 's'
            {
            match('s'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__7"

    // $ANTLR start "T__8"
    public final void mT__8() throws RecognitionException {
        try {
            int _type = T__8;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // exp0.g:6:6: ( '+' )
            // exp0.g:6:8: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__8"

    // $ANTLR start "T__9"
    public final void mT__9() throws RecognitionException {
        try {
            int _type = T__9;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // exp0.g:7:6: ( '-' )
            // exp0.g:7:8: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__9"

    // $ANTLR start "T__10"
    public final void mT__10() throws RecognitionException {
        try {
            int _type = T__10;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // exp0.g:8:7: ( '(' )
            // exp0.g:8:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__10"

    // $ANTLR start "T__11"
    public final void mT__11() throws RecognitionException {
        try {
            int _type = T__11;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // exp0.g:9:7: ( ')' )
            // exp0.g:9:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__11"

    // $ANTLR start "T__12"
    public final void mT__12() throws RecognitionException {
        try {
            int _type = T__12;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // exp0.g:10:7: ( 'x' )
            // exp0.g:10:9: 'x'
            {
            match('x'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__12"

    // $ANTLR start "T__13"
    public final void mT__13() throws RecognitionException {
        try {
            int _type = T__13;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // exp0.g:11:7: ( 'y' )
            // exp0.g:11:9: 'y'
            {
            match('y'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__13"

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // exp0.g:12:7: ( 'z' )
            // exp0.g:12:9: 'z'
            {
            match('z'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // exp0.g:13:7: ( '0' )
            // exp0.g:13:9: '0'
            {
            match('0'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // exp0.g:14:7: ( '1' )
            // exp0.g:14:9: '1'
            {
            match('1'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // exp0.g:15:7: ( '2' )
            // exp0.g:15:9: '2'
            {
            match('2'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // exp0.g:16:7: ( '3' )
            // exp0.g:16:9: '3'
            {
            match('3'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // exp0.g:17:7: ( '4' )
            // exp0.g:17:9: '4'
            {
            match('4'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // exp0.g:18:7: ( '5' )
            // exp0.g:18:9: '5'
            {
            match('5'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // exp0.g:19:7: ( '6' )
            // exp0.g:19:9: '6'
            {
            match('6'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // exp0.g:20:7: ( '7' )
            // exp0.g:20:9: '7'
            {
            match('7'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // exp0.g:21:7: ( '8' )
            // exp0.g:21:9: '8'
            {
            match('8'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // exp0.g:22:7: ( '9' )
            // exp0.g:22:9: '9'
            {
            match('9'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // exp0.g:29:5: ( ( ' ' | '\\t' | '\\n' | '\\r' )+ )
            // exp0.g:29:7: ( ' ' | '\\t' | '\\n' | '\\r' )+
            {
            // exp0.g:29:7: ( ' ' | '\\t' | '\\n' | '\\r' )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>='\t' && LA1_0<='\n')||LA1_0=='\r'||LA1_0==' ') ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // exp0.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);

             _channel=HIDDEN; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "WS"

    public void mTokens() throws RecognitionException {
        // exp0.g:1:8: ( T__5 | T__6 | T__7 | T__8 | T__9 | T__10 | T__11 | T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | WS )
        int alt2=21;
        switch ( input.LA(1) ) {
        case 'p':
            {
            alt2=1;
            }
            break;
        case ';':
            {
            alt2=2;
            }
            break;
        case 's':
            {
            alt2=3;
            }
            break;
        case '+':
            {
            alt2=4;
            }
            break;
        case '-':
            {
            alt2=5;
            }
            break;
        case '(':
            {
            alt2=6;
            }
            break;
        case ')':
            {
            alt2=7;
            }
            break;
        case 'x':
            {
            alt2=8;
            }
            break;
        case 'y':
            {
            alt2=9;
            }
            break;
        case 'z':
            {
            alt2=10;
            }
            break;
        case '0':
            {
            alt2=11;
            }
            break;
        case '1':
            {
            alt2=12;
            }
            break;
        case '2':
            {
            alt2=13;
            }
            break;
        case '3':
            {
            alt2=14;
            }
            break;
        case '4':
            {
            alt2=15;
            }
            break;
        case '5':
            {
            alt2=16;
            }
            break;
        case '6':
            {
            alt2=17;
            }
            break;
        case '7':
            {
            alt2=18;
            }
            break;
        case '8':
            {
            alt2=19;
            }
            break;
        case '9':
            {
            alt2=20;
            }
            break;
        case '\t':
        case '\n':
        case '\r':
        case ' ':
            {
            alt2=21;
            }
            break;
        default:
            NoViableAltException nvae =
                new NoViableAltException("", 2, 0, input);

            throw nvae;
        }

        switch (alt2) {
            case 1 :
                // exp0.g:1:10: T__5
                {
                mT__5(); 

                }
                break;
            case 2 :
                // exp0.g:1:15: T__6
                {
                mT__6(); 

                }
                break;
            case 3 :
                // exp0.g:1:20: T__7
                {
                mT__7(); 

                }
                break;
            case 4 :
                // exp0.g:1:25: T__8
                {
                mT__8(); 

                }
                break;
            case 5 :
                // exp0.g:1:30: T__9
                {
                mT__9(); 

                }
                break;
            case 6 :
                // exp0.g:1:35: T__10
                {
                mT__10(); 

                }
                break;
            case 7 :
                // exp0.g:1:41: T__11
                {
                mT__11(); 

                }
                break;
            case 8 :
                // exp0.g:1:47: T__12
                {
                mT__12(); 

                }
                break;
            case 9 :
                // exp0.g:1:53: T__13
                {
                mT__13(); 

                }
                break;
            case 10 :
                // exp0.g:1:59: T__14
                {
                mT__14(); 

                }
                break;
            case 11 :
                // exp0.g:1:65: T__15
                {
                mT__15(); 

                }
                break;
            case 12 :
                // exp0.g:1:71: T__16
                {
                mT__16(); 

                }
                break;
            case 13 :
                // exp0.g:1:77: T__17
                {
                mT__17(); 

                }
                break;
            case 14 :
                // exp0.g:1:83: T__18
                {
                mT__18(); 

                }
                break;
            case 15 :
                // exp0.g:1:89: T__19
                {
                mT__19(); 

                }
                break;
            case 16 :
                // exp0.g:1:95: T__20
                {
                mT__20(); 

                }
                break;
            case 17 :
                // exp0.g:1:101: T__21
                {
                mT__21(); 

                }
                break;
            case 18 :
                // exp0.g:1:107: T__22
                {
                mT__22(); 

                }
                break;
            case 19 :
                // exp0.g:1:113: T__23
                {
                mT__23(); 

                }
                break;
            case 20 :
                // exp0.g:1:119: T__24
                {
                mT__24(); 

                }
                break;
            case 21 :
                // exp0.g:1:125: WS
                {
                mWS(); 

                }
                break;

        }

    }


 

}