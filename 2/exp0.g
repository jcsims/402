grammar exp0;

@members {
public int numP = 0;
}
prog    :  WS? stmt+ WS?
    ;

stmt    :   'p' exp ';' {numP++;}
    |   's' lhsvar exp ';'
    ;

exp :   '+' exp exp
    |   '-' exp exp
    |   '(' exp ')'
    |   rhsvar
    |   num
    ;

lhsvar  :   'x' | 'y' | 'z'
    ;

rhsvar  :   'x' | 'y' | 'z'
    ;

num :   '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' |'9'
    ;

WS  : (' ' | '\t' | '\n' | '\r')+ { $channel=HIDDEN; }; // Ignoring whitespace!
    
