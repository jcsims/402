import org.antlr.runtime.*;

public class Exp0Driver {
    public static void main(String[] args)  throws Exception {
        if (args.length == 0) {
            System.out.println("Usage: java Exp0Driver <input file>");
            System.exit(0);
        } else {
            System.out.println("Processing: " + args[0]);
        }
        // set up and initialize our lexer and parser objects
        // open up the input file
        ANTLRFileStream input = new ANTLRFileStream(args[0]);
        // create the lexer with the input stream
        exp0Lexer lexer = new exp0Lexer(input);
        // create a token stream for the parser
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        // create a parser object with the token stream
        exp0Parser parser = new exp0Parser(tokens);
        // call the toplevel recursive descent parsing function -- start symbol
        parser.prog();
        
        //print out the number of p's encountered in the file
        System.out.println("Number of p's in the file: " + parser.numP);
    }
}
