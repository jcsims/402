// $ANTLR 3.2 debian-7 exp0.g 2013-09-26 10:08:01

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class exp0Parser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "WS", "'p'", "';'", "'s'", "'+'", "'-'", "'('", "')'", "'x'", "'y'", "'z'", "'0'", "'1'", "'2'", "'3'", "'4'", "'5'", "'6'", "'7'", "'8'", "'9'"
    };
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int EOF=-1;
    public static final int T__9=9;
    public static final int T__8=8;
    public static final int T__7=7;
    public static final int T__6=6;
    public static final int T__5=5;
    public static final int T__19=19;
    public static final int WS=4;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int T__10=10;

    // delegates
    // delegators


        public exp0Parser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public exp0Parser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return exp0Parser.tokenNames; }
    public String getGrammarFileName() { return "exp0.g"; }


    public int numP = 0;



    // $ANTLR start "prog"
    // exp0.g:6:1: prog : ( WS )? ( stmt )+ ( WS )? ;
    public final void prog() throws RecognitionException {
        try {
            // exp0.g:6:9: ( ( WS )? ( stmt )+ ( WS )? )
            // exp0.g:6:12: ( WS )? ( stmt )+ ( WS )?
            {
            // exp0.g:6:12: ( WS )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==WS) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // exp0.g:6:12: WS
                    {
                    match(input,WS,FOLLOW_WS_in_prog19); 

                    }
                    break;

            }

            // exp0.g:6:16: ( stmt )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==5||LA2_0==7) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // exp0.g:6:16: stmt
            	    {
            	    pushFollow(FOLLOW_stmt_in_prog22);
            	    stmt();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);

            // exp0.g:6:22: ( WS )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==WS) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // exp0.g:6:22: WS
                    {
                    match(input,WS,FOLLOW_WS_in_prog25); 

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "prog"


    // $ANTLR start "stmt"
    // exp0.g:9:1: stmt : ( 'p' exp ';' | 's' lhsvar exp ';' );
    public final void stmt() throws RecognitionException {
        try {
            // exp0.g:9:9: ( 'p' exp ';' | 's' lhsvar exp ';' )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==5) ) {
                alt4=1;
            }
            else if ( (LA4_0==7) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // exp0.g:9:13: 'p' exp ';'
                    {
                    match(input,5,FOLLOW_5_in_stmt44); 
                    pushFollow(FOLLOW_exp_in_stmt46);
                    exp();

                    state._fsp--;

                    match(input,6,FOLLOW_6_in_stmt48); 
                    numP++;

                    }
                    break;
                case 2 :
                    // exp0.g:10:9: 's' lhsvar exp ';'
                    {
                    match(input,7,FOLLOW_7_in_stmt60); 
                    pushFollow(FOLLOW_lhsvar_in_stmt62);
                    lhsvar();

                    state._fsp--;

                    pushFollow(FOLLOW_exp_in_stmt64);
                    exp();

                    state._fsp--;

                    match(input,6,FOLLOW_6_in_stmt66); 

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "stmt"


    // $ANTLR start "exp"
    // exp0.g:13:1: exp : ( '+' exp exp | '-' exp exp | '(' exp ')' | rhsvar | num );
    public final void exp() throws RecognitionException {
        try {
            // exp0.g:13:5: ( '+' exp exp | '-' exp exp | '(' exp ')' | rhsvar | num )
            int alt5=5;
            switch ( input.LA(1) ) {
            case 8:
                {
                alt5=1;
                }
                break;
            case 9:
                {
                alt5=2;
                }
                break;
            case 10:
                {
                alt5=3;
                }
                break;
            case 12:
            case 13:
            case 14:
                {
                alt5=4;
                }
                break;
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
                {
                alt5=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // exp0.g:13:9: '+' exp exp
                    {
                    match(input,8,FOLLOW_8_in_exp81); 
                    pushFollow(FOLLOW_exp_in_exp83);
                    exp();

                    state._fsp--;

                    pushFollow(FOLLOW_exp_in_exp85);
                    exp();

                    state._fsp--;


                    }
                    break;
                case 2 :
                    // exp0.g:14:9: '-' exp exp
                    {
                    match(input,9,FOLLOW_9_in_exp95); 
                    pushFollow(FOLLOW_exp_in_exp97);
                    exp();

                    state._fsp--;

                    pushFollow(FOLLOW_exp_in_exp99);
                    exp();

                    state._fsp--;


                    }
                    break;
                case 3 :
                    // exp0.g:15:9: '(' exp ')'
                    {
                    match(input,10,FOLLOW_10_in_exp109); 
                    pushFollow(FOLLOW_exp_in_exp111);
                    exp();

                    state._fsp--;

                    match(input,11,FOLLOW_11_in_exp113); 

                    }
                    break;
                case 4 :
                    // exp0.g:16:9: rhsvar
                    {
                    pushFollow(FOLLOW_rhsvar_in_exp123);
                    rhsvar();

                    state._fsp--;


                    }
                    break;
                case 5 :
                    // exp0.g:17:9: num
                    {
                    pushFollow(FOLLOW_num_in_exp133);
                    num();

                    state._fsp--;


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "exp"


    // $ANTLR start "lhsvar"
    // exp0.g:20:1: lhsvar : ( 'x' | 'y' | 'z' );
    public final void lhsvar() throws RecognitionException {
        try {
            // exp0.g:20:9: ( 'x' | 'y' | 'z' )
            // exp0.g:
            {
            if ( (input.LA(1)>=12 && input.LA(1)<=14) ) {
                input.consume();
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "lhsvar"


    // $ANTLR start "rhsvar"
    // exp0.g:23:1: rhsvar : ( 'x' | 'y' | 'z' );
    public final void rhsvar() throws RecognitionException {
        try {
            // exp0.g:23:9: ( 'x' | 'y' | 'z' )
            // exp0.g:
            {
            if ( (input.LA(1)>=12 && input.LA(1)<=14) ) {
                input.consume();
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "rhsvar"


    // $ANTLR start "num"
    // exp0.g:26:1: num : ( '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' );
    public final void num() throws RecognitionException {
        try {
            // exp0.g:26:5: ( '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' )
            // exp0.g:
            {
            if ( (input.LA(1)>=15 && input.LA(1)<=24) ) {
                input.consume();
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "num"

    // Delegated rules


 

    public static final BitSet FOLLOW_WS_in_prog19 = new BitSet(new long[]{0x00000000000000A0L});
    public static final BitSet FOLLOW_stmt_in_prog22 = new BitSet(new long[]{0x00000000000000B2L});
    public static final BitSet FOLLOW_WS_in_prog25 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_5_in_stmt44 = new BitSet(new long[]{0x0000000001FFF700L});
    public static final BitSet FOLLOW_exp_in_stmt46 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_6_in_stmt48 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_7_in_stmt60 = new BitSet(new long[]{0x0000000000007000L});
    public static final BitSet FOLLOW_lhsvar_in_stmt62 = new BitSet(new long[]{0x0000000001FFF700L});
    public static final BitSet FOLLOW_exp_in_stmt64 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_6_in_stmt66 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_8_in_exp81 = new BitSet(new long[]{0x0000000001FFF700L});
    public static final BitSet FOLLOW_exp_in_exp83 = new BitSet(new long[]{0x0000000001FFF700L});
    public static final BitSet FOLLOW_exp_in_exp85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_9_in_exp95 = new BitSet(new long[]{0x0000000001FFF700L});
    public static final BitSet FOLLOW_exp_in_exp97 = new BitSet(new long[]{0x0000000001FFF700L});
    public static final BitSet FOLLOW_exp_in_exp99 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_10_in_exp109 = new BitSet(new long[]{0x0000000001FFF700L});
    public static final BitSet FOLLOW_exp_in_exp111 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_exp113 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rhsvar_in_exp123 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_num_in_exp133 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_lhsvar0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_rhsvar0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_num0 = new BitSet(new long[]{0x0000000000000002L});

}