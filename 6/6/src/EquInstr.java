// implementation of the equ instruction

import java.util.*;

public class EquInstr extends Instr {

    private Stack<Integer> stack = null;

    public EquInstr(Stack<Integer> stack) {
        this.stack = stack;
    }


    public void executeInstr() {
        // implement the behavior
        if (stack.size() < 2) {
            System.out.println("Error: stack has less than 2 items, cannot equ!");
        } else {
            Integer temp = stack.pop();
            Integer temp2 = stack.pop();
            Integer result = (temp2 == temp) ? 1 : 0;
            stack.push(result);
            // transfer control to the next instuction
            executeNextInstr();
        }
    }
}
