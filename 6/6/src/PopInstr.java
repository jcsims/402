// implementation of the pop instruction

import java.util.*;

public class PopInstr extends Instr {

    private Stack<Integer> stack = null;

    public PopInstr(Stack<Integer> stack) {
        this.stack = stack;
    }


    public void executeInstr() {
        // implement the behavior
        if (stack.empty()) {
            System.out.println("Error: stack is empty, cannot pop!");
        } else {
            stack.pop();
            // transfer control to the next instuction
            executeNextInstr();
        }
    }
}
