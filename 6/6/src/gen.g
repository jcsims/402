tree grammar gen;

//************************************************************
// this is the code generation back end for our simple1 compiler

options{
    tokenVocab=simple1;     // use token definitions from simple1
    ASTLabelType=CommonTree;    // use CommonTree AST
    backtrack=true;         // we need backtracking because of if-then-else
}

@members{
    // generate unique label names
    private static int labelCnt = 0;
    private int getNewLabel() {
        return labelCnt++;
    }
}

prog returns [String code]
    :   {$code = "";}
        ^(STMTLIST (stmt {$code += $stmt.code;})+)
        {$code += "stop;\n";}
    ;

stmt returns [String code]
    :   ^('=' VAR exp)
        {
            $code = $exp.code + "store "+$VAR.text + ";\n";
        }
    |   ^('get' VAR)
        {
            // Ask for a value, then pop it off the stack into the var
            $code = "ask;\nstore " + $VAR.text + ";\n";
        }
    |   ^('put' exp)
        {
            $code = "push " + $exp.code + ";\n";
            $code += "print;\n";
        }
    |   ^('while' exp s=stmt)
        {
            String topLabel = "L"+getNewLabel();
            String bottomLabel = "L"+getNewLabel();
            $code = topLabel+":\n";
            $code += $exp.code + "jumpF " + bottomLabel+";\n";
            $code += $s.code;
            $code += "jump " + topLabel + ";\n";
            $code += bottomLabel + ":\nnoop;\n";
        }
    |   ^('if' exp s=stmt)
        {
            String endLabel = "L"+getNewLabel();
            $code = $exp.code + "jumpF " + endLabel + ";\n";
            $code += $s.code;
            $code += endLabel + ":\nnoop;\n";
        }
    |   ^('if' exp s1=stmt s2=stmt)
        {
            String elseLabel = "L"+getNewLabel();
            String endLabel = "L"+getNewLabel();
            $code = $exp.code + "jumpF " + elseLabel + ";\n";
            $code += $s1.code;
            $code += "jump " + endLabel + ";\n";
            $code += elseLabel + ":\n";
            $code += $s2.code;
            $code += endLabel + ":\nnoop;\n";
        }
    |   {$code = "";} ^(BLOCKSTMT (s=stmt {$code += $s.code;})+)
    ;

exp returns [String code]
    :   ^('==' e1=exp e2=exp)
        {$code = "push "+$e1.code+ ";\n"
            + "push "+$e2.code+";\n"
            + "equ;\n";}
    |   ^('<=' e1=exp e2=exp)
        {$code = "push "+$e1.code+ ";\n"
            + "push "+$e2.code+";\n"
            + "leq;\n";}
    |   ^('+' e1=exp e2=exp)
        {$code = "push "+$e1.code+ ";\n"
            + "push "+$e2.code+";\n"
            + "add;\n";}
    |   ^('-' e1=exp e2=exp)
        {$code = "push "+$e1.code+ ";\n"
            + "push "+$e2.code+";\n"
            + "sub;\n";}
    |   ^('*' e1=exp e2=exp)
        {$code = "push "+$e1.code+ ";\n"
            + "push "+$e2.code+";\n"
            + "mul;\n";}
    |   ^('/' e1=exp e2=exp)
        {$code = "push "+$e1.code+ ";\n"
            + "push "+$e2.code+";\n"
            + "div;\n";}
    |   VAR             {$code = $VAR.text;}
    |   NUM             {$code = $NUM.text;}
    ;
