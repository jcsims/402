// implementation of the ask instruction

import java.util.*;

public class AskInstr extends Instr {

    private Stack<Integer> stack = null;
    private String msg = null;
    private Integer value = null;

    public AskInstr(String msg, Stack<Integer> stack) {
        this.msg = msg;
        this.stack = stack;
    }


    public void executeInstr() {
        Scanner scanner = new Scanner(System.in);
        // implement the behavior
        if (msg != null) {
            System.out.println(msg);
        } else {
            System.out.println("Enter an integer to add to the stack:");
        }

        try {
            int input = scanner.nextInt();
            stack.push(new Integer(input));
            // transfer control to the next instuction
            executeNextInstr();
        } catch (NoSuchElementException e) {
            System.out.println("Error: invalid input to ask instruction");
        } catch (IllegalStateException e) {
            System.out.println("Error: unable to read standard input");
        }
    }
}
