// implementation of the dup instruction

import java.util.*;

public class DupInstr extends Instr {

    private Stack<Integer> stack = null;

    public DupInstr(Stack<Integer> stack) {
        this.stack = stack;
    }


    public void executeInstr() {
        // implement the behavior
        if (stack.empty()) {
            System.out.println("Error: stack is empty, cannot dup!");
        } else {
            Integer temp = stack.pop();
            stack.push(temp);
            stack.push(temp);
            // transfer control to the next instuction
            executeNextInstr();
        }
    }
}
