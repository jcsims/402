// implementation of the push instruction

import java.util.*;

public class PushInstr extends Instr {

    private Stack<Integer> stack = null;
    private String var = null;
    private Integer value = null;
    private HashMap<String, Integer> memory = null;

    public PushInstr(Integer value, Stack<Integer> stack) {
        this.value = value;
        this.stack = stack;
    }

    public PushInstr(String var, Stack<Integer> stack,
                     HashMap<String, Integer> memory) {
        this.var = var;
        this.stack = stack;
        this.memory = memory;
    }

    public void executeInstr() {
        // implement the behavior
        if (memory != null) {
            value = memory.get(var);
        }
        if (value != null) {
            stack.push(value);
            // transfer control to the next instuction
            executeNextInstr();
        } else {
            // This behavior is not defined in the spec
            // Defaulting to error condition
            System.out.println("Error: var " + var + "  is undefined");
        }
    }
}
