// implementation of the jumpF instruction

import java.util.*;

public class JumpFInstr extends Instr {

    private String label;
    private HashMap<String, Instr> labelTable;
    private Stack<Integer> stack;

    public JumpFInstr(String label, HashMap<String, Instr>
                      labelTable, Stack<Integer> stack) {
        this.label = label;
        this.labelTable = labelTable;
        this.stack = stack;
    }

    public void executeInstr() {
        // implement behavior
        if (stack.empty()) {
            System.out.println("Error: stack is empty, cannot jumpF!");
        } else {
            // if the top of the stack == 0 then transfer control to label
            // otherwise execute the next instruction in line
            if (stack.pop() == 0) {
                Instr i = (Instr)labelTable.get(label);
                // start executing this instruction
                i.executeInstr();
            }
            else {
                executeNextInstr();
            }
        }
    }
}
