// implementation of the sub instruction

import java.util.*;

public class SubInstr extends Instr {

    private Stack<Integer> stack = null;

    public SubInstr(Stack<Integer> stack) {
        this.stack = stack;
    }


    public void executeInstr() {
        // implement the behavior
        if (stack.size() < 2) {
            System.out.println("Error: stack has less than 2 items, cannot sub!");
        } else {
            Integer temp = stack.pop();
            Integer temp2 = stack.pop();
            stack.push(temp2 - temp);
            // transfer control to the next instuction
            executeNextInstr();
        }
    }
}
