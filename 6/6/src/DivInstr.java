// implementation of the div instruction

import java.util.*;

public class DivInstr extends Instr {

    private Stack<Integer> stack = null;

    public DivInstr(Stack<Integer> stack) {
        this.stack = stack;
    }


    public void executeInstr() {
        // implement the behavior
        if (stack.size() < 2) {
            System.out.println("Error: stack has less than 2 items, cannot div!");
        } else {
            Integer temp = stack.pop();
            Integer temp2 = stack.pop();
            if (temp2 == 0) {
                System.out.println("Error: division by zero!");
            } else {
                stack.push(temp2 / temp);
                // transfer control to the next instuction
                executeNextInstr();
            }
        }
    }
}
