// implementation of the print instruction

import java.util.Stack;

public class PrintInstr extends Instr {

    private Stack<Integer> stack;

    public PrintInstr(Stack<Integer> stack) {
        this.stack = stack;
    }

    public void executeInstr() {
        // implement the behavior of the instruction
        if (stack.empty()) {
            System.out.println("Error: stack is empty, cannot print!");
        } else {

            Integer num = stack.pop();
            System.out.println("Value: " + num);

            // this is required, it transfers control to the next instruction
            executeNextInstr();
        }
    }
}
